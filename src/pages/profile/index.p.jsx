import React, { useState } from 'react';
import SideBar from './sidebar'
import Content from './content'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: "flex",
      flexDirection: "row",
    },
    sidebarWrapper: {
      maxWidth: 337,
      minWidth: 337,
      [theme.breakpoints.down('sm')]: {
        maxWidth: '100vw',
        minWidth: '100vw'
      }
    },
    contentWrapper: {
      display: 'flex',
      flexGrow: 1,
      flexDirection: 'column',
      [theme.breakpoints.down('sm')]: {
        display: 'none'
      }
    }
  })
);

function Index() {

  const classes = useStyles()
  const [activeTab, setActiveTab] = useState("Info")

  return (
    <div className={classes.root}>
      <div className={classes.sidebarWrapper}>
        <SideBar activeTab={activeTab} setActiveTab={setActiveTab} />
      </div>
      <div className={classes.contentWrapper}>
        <Content activeTab={activeTab} setActiveTab={setActiveTab} />
      </div>
    </div>
  );
}

export default Index;