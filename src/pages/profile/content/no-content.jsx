import React from 'react';
import AddPostImage from '../../../components/assets/svgs/add-post-image'
import AddPostImageWrapper from '../../../components/assets/svgs/add-post-image-wrapper'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'relative',
      minHeight:'90vh',
      flexGrow: 1,
      '& span': {
        position: 'absolute'
      }
    }
  })
);

function NoContent() {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <AddPostImageWrapper />
      <AddPostImage />
    </div>
  );
}

export default NoContent;