import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      listStyle: 'none',
      display: 'inline-flex',
      paddingLeft: theme.spacing(3)
    },
    tab: {
      cursor: 'pointer',
      borderRight: '1px solid #c4c4c4',
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 8,
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      color: '#707070',
      '&[data-active="true"]': {
        color: '#ce0f3d'
      }
    }
  })
);

function Nav({ activeTab, setActiveTab }) {

  const classes = useStyles()

  const handleTabClick = (title) => {
    setActiveTab(() => title)
  }

  return (
    <ul className={classes.root}>
      <li
        data-active={activeTab == 'Publish'}
        onClick={() => handleTabClick('Publish')}
        className={classes.tab}>Publish</li>
      <li
        data-active={activeTab == 'Draft'}
        onClick={() => handleTabClick('Draft')}
        className={classes.tab}>Draft</li>
      <li
        data-active={activeTab == 'Save'}
        onClick={() => handleTabClick('Save')}
        className={classes.tab}>Save</li>
      <li
        data-active={activeTab == 'My Project'}
        onClick={() => handleTabClick('My Project')}
        className={classes.tab}>My Project</li>
    </ul>
  );
}

export default Nav;