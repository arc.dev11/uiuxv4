import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column'
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingTop: 4,
            paddingBottom: 4
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16
        }
    })
);

function Education() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.row}>
                <span className={classes.title}>Master, Soore Art</span>
                <span className={classes.title}>2019</span>
            </div>
            <div className={classes.row}>
                <span className={classes.title}>Bachelor, Nabi Akram Art</span>
                <span className={classes.title}>2019</span>
            </div>
        </div>
    );
}

export default Education;