import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 14
        }
    })
);

function AboutMe() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            I received a bachelor's degree in Graphic Design in 2012 and I have 12 years related work experience.<br />
            I have been working professionally in Interface Design for 6 years.<br />
            I have a detailed knowledge of Icon design, vector design and logo design, Material design system and Flat Design system.<br />
            I was involved in various projects during my bachelor’s program.<br />
        </div>
    );
}

export default AboutMe;