
import React from 'react';
import KeyboardArrowDownIcon from '../../../../components/assets/svgs/keyboard-arrow-down-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: ' baseline'
        },
        title: {
            paddingRight: 8,
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 12,
            textAlign: 'center',
            color: '#ce0f3d'
        }
    })
);

function ProfileInfoFooter({ title }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <span className={classes.title}>{title}</span>
            <KeyboardArrowDownIcon />
        </div>
    );
}

export default ProfileInfoFooter;