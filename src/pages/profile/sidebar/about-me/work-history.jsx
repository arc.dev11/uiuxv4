import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column'
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingTop: 4,
            paddingBottom: 4
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16
        }
    })
);

function WorkHistory() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.row}>
                <span className={classes.title}>UI/UX Designer at Safarzon</span>
                <span className={classes.title}>2017–2019</span>
            </div>
            <div className={classes.row}>
                <span className={classes.title}>UI/UX Designer at Samim Rayane</span>
                <span className={classes.title}>2016–2017</span>
            </div>
        </div>
    );
}

export default WorkHistory;