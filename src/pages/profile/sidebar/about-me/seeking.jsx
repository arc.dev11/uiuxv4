import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column'
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingTop: 4,
            paddingBottom: 4
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16
        }
    })
);

function Seeking() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.row}>
                <span className={classes.title}>Freelance or contract opportunities</span>
                <span className={classes.title}></span>
            </div>
            <div className={classes.row}>
                <span className={classes.title}>Full-time opportunities</span>
                <span className={classes.title}></span>
            </div>
        </div>
    );
}

export default Seeking;