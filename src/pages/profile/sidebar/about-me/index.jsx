import React from 'react';
import Header from './header'
import Footer from './footer'
import AboutMe from './about-me'
import Skill from './skills'
import WorkHistory from './work-history'
import Education from './education'
import Seeking from './seeking'
import Divider from '../divider'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column'
        }
    })
);

function ProfileInfo(props) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Header title="ABOUT ME" />
            <AboutMe />
            <Footer title="Read More" />
            <Divider />
            <Header title="Skills" />
            <Skill />
            <Footer title="Read More" />
            <Divider />
            <Header title="Work History" />
            <WorkHistory />
            <Footer title="Show Detail" />
            <Divider />
            <Header title="Education" />
            <Education />
            <Divider />
            <Header title="Seeking" />
            <Seeking />
        </div>
    );
}

export default ProfileInfo;