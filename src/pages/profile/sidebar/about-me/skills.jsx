import React from 'react';
import AdobeXdIcon from '../../../../components/assets/svgs/adobe-xd-icon'
import CircleCheckIcon from '../../../../components/assets/svgs/circle-check-icon'
import FigmaIcon from '../../../../components/assets/svgs/figma-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            paddingTop: 8,
            paddingBottom: 8
        },
        item: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        leftSide: {
            display: 'flex'
        },
        title: {
            paddingLeft: 8,
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16
        }
    })
);

function Skills() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.item}>
                <div className={classes.leftSide}>
                    <AdobeXdIcon />
                    <span className={classes.title}>Adobe XD</span>
                </div>
                <CircleCheckIcon />
            </div>
            <div className={classes.item}>
                <div className={classes.leftSide}>
                    <FigmaIcon />
                    <span className={classes.title}>Figma</span>
                </div>
                <CircleCheckIcon />
            </div>
        </div>
    );
}

export default Skills;