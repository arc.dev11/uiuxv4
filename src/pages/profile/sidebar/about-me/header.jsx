
import React from 'react';
import EditIcon from '../../../../components/assets/svgs/edit-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingTop: 8
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18
        }
    })
);

function ProfileInfoHeader({ title }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <span className={classes.title}>{title}</span>
            <EditIcon color="#CE0F3D" />
        </div>
    );
}

export default ProfileInfoHeader;