import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            cursor: 'pointer',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 8
        },
        icon: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            paddingRight: 8
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 12,
            textAlign: 'center',
            color: '#707070',
            '&[data-active="true"]': {
                color: '#ce0f3d'
            }
        }
    })
);

function NavItem({ title, icon: Icon, active = false, handleTabClick }) {

    const classes = useStyles()

    return (
        <div
            className={classes.root}
            onClick={() => handleTabClick(title)}>
            <Icon className={classes.icon} color={active ? '#CE0F3D' : '#707070'} />
            <span
                data-active={active}
                className={classes.title}>
                {title}
            </span>
        </div>
    );
}

export default NavItem;