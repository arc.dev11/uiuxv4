import React from 'react';
import NavItem from './item'
import InfoIcon from '../../../../components/assets/svgs/info-icon'
import SaveIcon from '../../../../components/assets/svgs/save-icon'
import PublishIcon from '../../../../components/assets/svgs/publish-icon'
import DraftIcon from '../../../../components/assets/svgs/draft-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            paddingTop: 4,
            paddingBottom: 4
        }
    })
);

function Nav({ activeTab, setActiveTab }) {

    const classes = useStyles()
    const handleTabClick = (title) => {
        setActiveTab(() => title)
    }

    return (
        <div className={classes.root}>
            <NavItem handleTabClick={handleTabClick} icon={InfoIcon} title="Info" active={activeTab == "Info"} />
            <NavItem handleTabClick={handleTabClick} icon={PublishIcon} title="Publish" active={activeTab == "Publish"} />
            <NavItem handleTabClick={handleTabClick} icon={DraftIcon} title="Draft" active={activeTab == "Draft"} />
            <NavItem handleTabClick={handleTabClick} icon={SaveIcon} title="Save" active={activeTab == "Save"} />
        </div>
    );
}

export default Nav;