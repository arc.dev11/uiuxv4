import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingTop: 4,
            paddingBottom: 4
        },
        column: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 14,
            textAlign: 'center'
        },
        subtitle: {
            paddingTop: 8,
            paddingBottom: 8,
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18,
            textAlign: 'center',
            color: '#ce0f3d'
        }
    })
);


function Statistic() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.column}>
                <div className={classes.title}>Projects</div>
                <div className={classes.subtitle}>0</div>
            </div>
            <div className={classes.column}>
                <div className={classes.title}>Posts</div>
                <div className={classes.subtitle}>0</div>
            </div>
            <div className={classes.column}>
                <div className={classes.title}>Followers</div>
                <div className={classes.subtitle}>0</div>
            </div>
            <div className={classes.column}>
                <div className={classes.title}>Following</div>
                <div className={classes.subtitle}>0</div>
            </div>
        </div>
    );
}

export default Statistic;