import React from 'react';
import EditIcon from '../../../components/assets/svgs/edit-icon'
import VerifiedUserIcon from '../../../components/assets/svgs/verified-user-icon'
import ProIcon from '../../../components/assets/svgs/pro-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";


const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            '& span': {
                paddingLeft: 4,
                paddingRight: 4
            }
        },
        leftSide: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 18,
            color: '#ce0f3d'
        }
    })
);


function TopBar() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.leftSide}>
                <EditIcon color="#ce0f3d" />
                <span className={classes.title}>Edit Profile</span>
            </div>
            <div>
                <ProIcon color="#707070" />
                <VerifiedUserIcon color="#707070" />
            </div>
        </div>
    );
}

export default TopBar;