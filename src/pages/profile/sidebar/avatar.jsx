import React from 'react';
import ProfileImage from '../../../components/assets/svgs/profile-image'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            paddingTop: 8,
            paddingBottom: 8
        },
        info: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            paddingLeft: 8
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18
        },
        subtitle: {
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16
        }
    })
);

function Avatar() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <ProfileImage />
            <div className={classes.info}>
                <div className={classes.title}>Jose Bradley</div>
                <div className={classes.subtitle}>Istanbul / Turkey</div>
            </div>
        </div>
    );
}

export default Avatar;