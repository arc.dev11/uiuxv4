import React from 'react';
import { LinearProgress } from '@material-ui/core';
import AboutMeImage from '../../../components/assets/svgs/about-me-image'
import { makeStyles, createStyles } from "@material-ui/core/styles";
import Divider from './divider'

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
        },
        progressTitle: {
            padding: theme.spacing(0.5),
            width: '100%',
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 12,
            textAlign: 'left'
        },
        progressWrapper: {
            padding: theme.spacing(0.5),
            width: '100%',
            display: 'inline-flex',
            alignItems: 'center',
            '& span': {
                paddingRight: theme.spacing(1),
                paddingLeft: theme.spacing(1),
                fontStyle: 'normal',
                fontWeight: 'bold',
                fontSize: 12,
                textAlign: 'center',
                color: theme.palette.primary.main
            },
            '& .MuiLinearProgress-bar': {
                borderRadius: 10
            }
        },
        progress: {
            flexGrow: 1,
            borderRadius: 10,
            background: theme.palette.secondary.main
        },
        button: {
            width: 229,
            height: 47,
            background: '#ce0f3d',
            borderRadius: 25,
            fonStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18,
            textAlign: 'center',
            color: '#ffffff'
        },
        title: {
            textAlign: 'center',
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18
        },
        subtitle: {
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16,
            textAlign: 'center',
            padding: 15
        },
        imageWrapper: {
            position: 'relative',
            display: 'flex',
            justifyContent: 'center',
            height: 320,
            width: 320
        },
        image: {
            position: 'absolute'
        }
    })
);


function AboutMePlaceholder({ setStart }) {

    const classes = useStyles()
    const handleClick = () => {
        setStart(() => true)
    }

    return (
        <div className={classes.root}>
            <div className={classes.progressTitle}>Account completion</div>
            <div className={classes.progressWrapper}>
                <LinearProgress
                    classes={{ root: classes.progress }}
                    variant="determinate"
                    color="primary"
                    value={12} />
                <span>{`${Math.round(12)}%`}</span>
            </div>
            <Divider />
            <div className={classes.imageWrapper}>
                <AboutMeImage className={classes.image} />
            </div>
            <div className={classes.title}>Your Account Is Not Complete</div>
            <div className={classes.subtitle}>How about completing your account?<br />this will be very good for you</div>
            <button
                onClick={handleClick}
                className={classes.button}>
                Start Completing
            </button>
        </div>
    );
}

export default AboutMePlaceholder