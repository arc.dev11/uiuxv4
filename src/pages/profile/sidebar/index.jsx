import React, { useState } from 'react';
import TopBar from './top-bar'
import Avatar from './avatar'
import Statistic from './statistic'
import SocialMedia from './social-media'
import AboutMePlaceholder from './about-me-placeholder'
import AboutMe from './about-me'
import Divider from './divider'
import Nav from './nav'
import { Hidden } from '@material-ui/core'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column'
    }
  })
);

function SideBar({ activeTab, setActiveTab }) {
  const classes = useStyles()
  const [start, setStart] = useState(false)
  return (
    <div className={classes.root}>
      <TopBar />
      <Avatar />
      <Statistic />
      <Hidden mdUp implementation="css">
        <Divider />
        <Nav activeTab={activeTab} setActiveTab={setActiveTab} />
      </Hidden>
      <Divider />
      <SocialMedia />
      <Divider />
      <Hidden mdUp implementation="css">
        {activeTab === 'Info' && (
          <>
            {start ? <AboutMe /> : <AboutMePlaceholder setStart={setStart} />}
          </>
        )}
      </Hidden>
      <Hidden smDown implementation="css">
        {start ? <AboutMe /> : <AboutMePlaceholder setStart={setStart} />}
      </Hidden>
    </div>
  );
}

export default SideBar;