import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            borderBottom: '2px solid #c8c8c8',
            width:'100%'
        }
    })
);

function Divider() {
    const classes = useStyles()
    return (
        <div className={classes.root}></div>
    );
}

export default Divider;