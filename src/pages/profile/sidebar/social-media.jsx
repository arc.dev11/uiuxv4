import React from 'react';
import BehanceIcon from '../../../components/assets/svgs/behance-icon'
import DribbbleIcon from '../../../components/assets/svgs/dribbble-icon'
import GithubIcon from '../../../components/assets/svgs/github-icon'
import GitlabIcon from '../../../components/assets/svgs/gitlab-icon'
import LinkedinIcon from '../../../components/assets/svgs/linkedin-icon'
import PinterestIcon from '../../../components/assets/svgs/pinterest-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            '& span': {
                display: 'flex',
                justifyContent: 'center',
                padding: 8
            }
        }
    })
);

function SocialMedia() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <BehanceIcon />
            <DribbbleIcon />
            <PinterestIcon />
            <LinkedinIcon />
            <GithubIcon />
            <GitlabIcon />
        </div>
    );
}

export default SocialMedia;