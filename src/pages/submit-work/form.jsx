import React from 'react';
import dynamic from 'next/dynamic'
import ProAccountButton from './pro-account-button';
import TagsInput from '../../components/formik-custom-fields/tags-input';
import { makeStyles, Button } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  textarea: {
    padding: 20,
    background: '#f3f3f3',
    borderRadius: 20,
    color: '#ce0f3d',
    width: '100%',
    height: 112,
    resize: 'none'
  },
  tagsWrapper: {
    paddingRight: 20,
    paddingLeft: 20
  },
  tag: {
    cursor: 'pointer',
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    marginRight: 8,
    padding: 8,
    background: '#f3f3f3',
    borderRadius: 15,
    fontStyle: 'normal',
    fontSize: 16,
    color: '#ce0f3d'
  },
  label: {
    display: 'block',
    padding: 10,
    paddingLeft: 30,
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: 18
  },
  input: {
    paddingRight: 20,
    paddingLeft: 20,
    background: '#f3f3f3',
    borderRadius: 28,
    height: 56,
    width: '100%',
    color: '#ce0f3d'
  },
  buttonWrapper: {
    marginTop: theme.spacing(3),
    width:'100%',
    display: 'inline-flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: theme.spacing(3)
  },
  button: {
    width:'50%'
  }
}));

const MultiSelect = dynamic(
  () => import('../../components/formik-custom-fields/multi-select'),
  { ssr: false }
)

const Form = ({
  errors,
  values,
  setFieldValue,
  handleSubmit,
  handleChange,
  handleBlur
}) => {

  const classes = useStyles()
  const onKeyDown = keyEvent => {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  return (
    <form onSubmit={handleSubmit} onKeyDown={onKeyDown}>

      <label className={classes.label}>Title</label>
      <input
        className={classes.input}
        type="text"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.title}
        name="title"
      />
      {errors.title && <div>{errors.title}</div>}

      <label className={classes.label}>Creative Fields</label>
      <MultiSelect
        uniqueKey={'value'}
        data={[
          {
            label: "One",
            value: 1
          },
          {
            label: "Two",
            value: 2
          },
          {
            label: "Three",
            value: 3
          }
        ]}
        selected={values.creativeFields}
        toggleChangeListItem={dta => {
          setFieldValue('creativeFields', Array.from(new Set([...values.creativeFields, dta])))
        }}
        removeChangeListItem={dta => {
          setFieldValue('creativeFields', values.creativeFields.filter(el => el !== dta))
        }}
      />
      {errors.creativeFields && <div>{errors.creativeFields}</div>}


      <label className={classes.label}>Tools Used</label>
      <MultiSelect
        uniqueKey={'value'}
        data={[
          {
            label: "One",
            value: 1
          },
          {
            label: "Two",
            value: 2
          },
          {
            label: "Three",
            value: 3
          }
        ]}
        selected={values.tools}
        toggleChangeListItem={dta => {
          setFieldValue('tools', Array.from(new Set([...values.tools, dta])))
        }}
        removeChangeListItem={dta => {
          setFieldValue('tools', values.tools.filter(el => el !== dta))
        }}
      />
      {errors.tools && <div>{errors.tools}</div>}


      <label className={classes.label}>Add Team Member</label>
      <ProAccountButton />

      <label className={classes.label}>Tags</label>
      <TagsInput
        onChange={tags => {
          setFieldValue('tags', tags)
        }}
        value={values.tags}
      />
      {errors.tools && <div>{errors.tags}</div>}

      <label className={classes.label}>SUGGESTED TAGS</label>
      <div className={classes.tagsWrapper}>
        {['graphic design', 'Adobe XD', 'Figma', 'UIUX', 'UXUI', 'User Interface', 'User Experience'].map(el => {
          return (
            <div
              onClick={() => {
                setFieldValue('tags', Array.from(new Set([...values.tags, el])))
              }}
              key={el}
              className={classes.tag}>
              {el}
            </div>
          )
        })}
      </div>

      <label className={classes.label}>Description</label>
      <textarea
        className={classes.textarea}
        type="text"
        onChange={handleChange}
        onBlur={handleBlur}
        value={values.description}
        name="description"
      />
      {errors.description && <div>{errors.description}</div>}

      <div className={classes.buttonWrapper}>
        <Button 
        className={classes.button}
        type="submit" 
        variant="contained" 
        color="secondary">
          SAVE TO DRAFT
        </Button>
        <Button 
        className={classes.button}
        type="submit" 
        variant="contained" 
        color="primary">
          Publish
        </Button>
      </div>

    </form>
  )
};

export default Form