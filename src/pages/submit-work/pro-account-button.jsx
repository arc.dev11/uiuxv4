import React from 'react';
import ProIcon from '../../components/assets/svgs/pro-icon';
import { makeStyles } from "@material-ui/core";
import Theme from '../../theme'

const useStyles = makeStyles(theme => ({
  root: {
    fontStyle: 'normal',
    fontSize: 18,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#fae6eb',
    borderRadius: 28,
    minHeight: 56,
    width: '100%'
  },
  icon: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  }
}));


const ProAccountButton = () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      Special for Pro Account
      <ProIcon color={Theme.palette.primary.main} className={classes.icon} />
    </div>
  )
};

export default ProAccountButton