import { Hidden } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import AvailableExtensionsVertical from './available-extensions/vertical-view'
import AvailableExtensionsHorizontal from './available-extensions/horizontal-view'
import Editor from './editor'
import Footer from './footer';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%'
    },
    editorWrapper: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%'
    }
  })
);

const Photo = ({ value, onChange }) => {

  const classes = useStyles()
  const [state, setState] = useState(value);

  useEffect(() => {
    onChange(state)
  }, [state])

  const createID = () => {
    return Array(16)
      .fill(0)
      .map(() => String.fromCharCode(Math.floor(Math.random() * 26) + 97))
      .join('') +
      Date.now().toString(24);
  }

  const handleImageDelete = () => {
    setState(odt => {
      return {
        selected: null,
        images: odt.images.filter(el => el.id !== odt.selected)
      }
    })
  }

  const handleImageChange = base64Image => {
    if (base64Image) {
      if (state.selected === null) {
        setState(odt => {
          const id = createID();
          return {
            selected: id,
            images: [
              ...odt.images, {
                id,
                src: base64Image
              }
            ]
          }
        })
      }
      else {
        setState(odt => {
          return {
            ...odt,
            images: odt.images.map(el => {
              if (el.id === state.selected) {
                return {
                  ...el,
                  src: base64Image
                }
              }
              return el
            })
          }
        })
      }
    }
  }


  return (
    <div className={classes.root}>
      <Hidden smDown implementation="css">
        <AvailableExtensionsVertical />
      </Hidden>

      <div className={classes.editorWrapper}>
        <Hidden mdUp implementation="css">
          <AvailableExtensionsHorizontal />
        </Hidden>

        <Editor
          src={state.selected === null ? null : state.images.find(el => el.id === state.selected).src}
          onDelete={handleImageDelete}
          onChange={handleImageChange} />
        {
          state.images.length > 0 && (
            <Footer
              onAddButtonClick={() => {
                if (state.images.length < 3) {
                  setState(odt => {
                    const id = createID();
                    return {
                      selected: id,
                      images: [
                        ...odt.images, {
                          id,
                          src: null
                        }
                      ]
                    }
                  })
                }
                else {
                  // show pro needed dialog alert
                }
              }}
              images={state.images}
              onImageClick={selected => {
                setState(odt => {
                  return {
                    ...odt,
                    selected: selected.id
                  }
                })
              }} />
          )
        }
      </div>
    </div>
  );
};

export default Photo;