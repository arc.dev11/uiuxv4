import React from 'react'
import UploadPlaceHolderImage from '../../../../components/assets/svgs/upload-placeholder-image'
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { Grid, Hidden } from '@material-ui/core';

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            display: 'flex',
            flexDirection: 'column',
        },
        main: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            flexGrow: 1
        },
        image: {
            padding: 15
        },
        title: {
            padding: 15,
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 22,
            textAlign: 'center',
            [theme.breakpoints.down('sm')]: {
                fontSize: 18
            }
        },
        button: {
            width: 225,
            height: 56,
            borderRadius: 28,
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18,
            textAlign: 'center',
            zIndex: 1,
            backgroundColor: '#ce0f3d',
            color: '#ffffff'
        },
        divider: {
            borderBottom: '2px solid #ce0f3d',
            width: '95%',
            margin: 'auto'
        },
        footer: {
            width: '95%',
            margin: 'auto',
            height: '10%',
            marginBottom: theme.spacing(2)
        },
        footerItem: {
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16
        },
        square: {
            marginRight: theme.spacing(1),
            marginLeft: theme.spacing(1),
            background: '#CE0F3D',
            width: 8,
            height: 8
        }
    })
);

const PlaceHolder = ({ handleClick }) => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.main}>
                <UploadPlaceHolderImage
                    width="20vw"
                    height="auto"
                    className={classes.image} />
                <div className={classes.title}>Drag and drop Your Work Or</div>
                <button onClick={handleClick} className={classes.button}>Browse</button>
            </div>

            <Hidden smDown implementation="css">
                <div className={classes.divider}></div>
                <Grid container className={classes.footer}>
                    <Grid className={classes.footerItem} item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <span className={classes.square} /> 1440 x 1080 or higher recommended. MAX size 10MB
                    </Grid>
                    <Grid className={classes.footerItem} item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <span className={classes.square} /> Animation GIF file size: 900 x 675
                    </Grid>
                    <Grid className={classes.footerItem} item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <span className={classes.square} /> High resolution images (png, jpg, gif)
                    </Grid>
                    <Grid className={classes.footerItem} item xs={12} sm={12} md={6} lg={6} xl={6}>
                        <span className={classes.square} /> Video file size: 4:3 max size 20MB
                    </Grid>
                </Grid>
            </Hidden>
        </div>
    )
}

export default PlaceHolder

