import React, { useState, useRef, useEffect } from 'react'
import DragDrop from './dragdrop'
import PlaceHolder from './placeholder'
import ImageCrop from './image-crop'
import ImageView from './image-view'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            position: 'relative',
            width: '95%',
            margin:'auto',
            paddingTop: '75%',
            backgroundColor: theme.palette.secondary.main,
            borderRadius: 20
        }
    })
);

const Editor = ({ src = null, onDelete, onChange }) => {

    const classes = useStyles()
    const fileInput = useRef()
    const [editMode, setEditMode] = useState(false)
    const [currentImage, setCurrentImage] = useState(src)

    useEffect(() => {
        setCurrentImage(() => src)
    }, [src])

    useEffect(() => {
        setEditMode(() => false)
        onChange(currentImage)
    }, [currentImage])

    const handleChange = (e) => {
        if (e.target.files && e.target.files[0]) {
            let reader = new FileReader();
            reader.onload = function (ev) {
                setCurrentImage(ev.target.result)
            }.bind(this);
            reader.readAsDataURL(e.target.files[0]);
        }
    }


    const onFilesDropped = (files) => {
        let reader = new FileReader();
        reader.onload = function (ev) {
            setCurrentImage(ev.target.result)
        };
        reader.readAsDataURL(files[0]);
    }

    return (
        <div className={classes.root}>
            <input
                style={{ display: 'none' }}
                ref={fileInput}
                type="file"
                onChange={handleChange} />

            {currentImage !== null ? (
                <>
                    {editMode ? (
                        <ImageCrop
                            onAccept={setCurrentImage}
                            src={currentImage} />
                    ) : (
                        <ImageView
                            src={currentImage}
                            onDelete={onDelete}
                            onEdit={() => setEditMode(() => true)}
                        />
                    )}
                </>
            ) : (
                <>
                    <PlaceHolder handleClick={() => fileInput.current.click()} />
                    <DragDrop onFilesDropped={onFilesDropped} />
                </>
            )}
        </div>
    )
}

export default Editor

