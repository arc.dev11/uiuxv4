import React, { useState, useCallback, useRef } from 'react'
import Cropper from 'react-easy-crop'
import getCroppedImg from './croper'
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      borderRadius: 20,
      position: 'absolute',
      top: 0,
      bottom: 0,
      right: 0,
      left: 0,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    controls: {
      position: 'absolute',
      bottom: '5%',
      left: '50%',
      width: '50%',
      transform: 'translateX(-50%)',
      height: 80,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      background: 'rgba(0, 0, 0, 0.5)',
      borderRadius: '20px 20px 20px 0px',
      width: 386,
      height: 47,
      '& input': {
        flexGrow: 1,
        marginLeft: 8,
        marginRight: 8
      },
      '& button': {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        color: '#ffffff',
        background: '#ce0f3d',
        borderRadius: 20,
        width: 140,
        height: 47
      }
    }
  })
);


const ImageCrop = ({ src, onAccept }) => {

  const classes = useStyles()
  const [loading, setLoading] = useState(false)
  const [crop, setCrop] = useState({ x: 0, y: 0 })
  const [zoom, setZoom] = useState(1)

  const vars = useRef({
    croppedAreaPixels: {}
  }).current


  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    vars.croppedAreaPixels = croppedAreaPixels
  }, [])

  const addCroppedImage = useCallback(async () => {
    try {
      setLoading(() => true)
      const croppedImage = await getCroppedImg(
        src,
        vars.croppedAreaPixels
      )
      setLoading(() => false)
      onAccept(croppedImage)

    } catch (e) {
      console.error(e)
    }
  }, [vars.croppedAreaPixels])

  return (
    <div className={classes.root}>

      <Cropper
        style={{ containerStyle: { backgroundColor: '#E4ECEE' } }}
        image={src}
        crop={crop}
        zoom={zoom}
        aspect={4 / 3}
        onCropChange={setCrop}
        onCropComplete={onCropComplete}
        onZoomChange={setZoom}
      />

      <div className={classes.controls}>
        <input
          type="range"
          value={zoom}
          min={1}
          max={3}
          step={0.1}
          onChange={e => setZoom(e.target.value)}
        />
        <button
          disabled={loading}
          onClick={addCroppedImage}>
          <span style={{ paddingRight: 8 }}>Accept</span>{loading && <CircularProgress color="secondary" />}
        </button>
      </div>
    </div>
  )
}

export default ImageCrop
