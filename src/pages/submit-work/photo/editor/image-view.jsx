import React from 'react'
import TrashIcon from '../../../../components/assets/svgs/trash-icon'
import CropIcon from '../../../../components/assets/svgs/crop-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            backgroundColor: '#f3f3f3',
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'center',
            [theme.breakpoints.down('sm')]: {
                borderRadius: 10
            },
            '& img': {
                borderRadius: 20,
                width: '100%',
                height: '100%',
                opacity: 0.6
            },
            '& span': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                right: 0,
                bottom: 0,
                background: '#707070',
                borderRadius: '20px 20px 20px 0px',
                width: 80,
                height: 80,
                '&:nth-child(2)': {
                    borderRadius: '0px 20px 20px 20px',
                    top: 0,
                    bottom: 'auto'
                }
            }
        }
    })
);

const ImageView = ({ src, onDelete, onEdit }) => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <img src={src} />
            <span onClick={onEdit}><CropIcon /></span>
            <span onClick={onDelete}><TrashIcon /></span>
        </div>
    )
}

export default ImageView