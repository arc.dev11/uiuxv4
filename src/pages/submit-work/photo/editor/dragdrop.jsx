import React from 'react'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            position: 'absolute',
            top: 0,
            bottom: 0,
            right: 0,
            left: 0
        }
    })
);


const DropDrop = (props) => {
    const classes = useStyles()
    const onDrop = e => {
        e.preventDefault()
        let files = mapFileListToArray(e.dataTransfer.files)
        props.onFilesDropped(files)
    }

    const onDragOver = e => {
        e.preventDefault()
    }

    const mapFileListToArray = fileList => {
        let fl = fileList.length
        let files = []
        for (let i = 0; i < fl; i++) {
            files.push(fileList[i])
        }
        return files
    }

    return <div className={classes.root} onDrop={onDrop} onDragOver={onDragOver} />
}

export default DropDrop