import React from 'react';
import ProIcon from '../../../components/assets/svgs/pro-icon';
import AddIcon from '../../../components/assets/svgs/add-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";
import theme from '../../../theme'

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            paddingTop: theme.spacing(2),
            width: '95%',
            margin:'auto',
            display: 'flex',
            flexDirection: 'column'
        },
        topbar: {
            display: 'flex',
            flexDirection: 'row',
            padding: 8,
            alignItems: 'center',
            justifyContent: 'flex-start'
        },
        title: {
            paddingLeft: 16,
            paddingRight: 16,
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 22,
            color: theme.palette.primary.main
        },
        imageWrapper: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            flexWrap: 'wrap'
        },
        image: {
            marginTop: 8,
            marginRight: 8,
            borderRadius: 20,
            width: 167,
            height: 125
        },
        addButton: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 8,
            background: '#f3f3f3',
            borderRadius: 20,
            width: 167,
            height: 125
        },
        proAccountIcon: {
            paddingLeft: 8,
            paddingRight: 8
        }
    })
);

function Footer({ images, onImageClick, onAddButtonClick }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.topbar}>
                <div className={classes.title}>Multi Image</div>
                Special for Pro Account
                <ProIcon color={theme.palette.primary.main} className={classes.proAccountIcon} />
            </div>
            <div className={classes.imageWrapper}>
                {
                    React.Children.toArray(images.map((el) => {
                        return (
                            <img
                                alt=""
                                onClick={() => onImageClick(el)}
                                className={classes.image}
                                src={el.src} />
                        )
                    }))
                }
                <div
                    onClick={onAddButtonClick}
                    className={classes.addButton}>
                    <AddIcon />
                </div>
            </div>
        </div>
    );
}

export default Footer;