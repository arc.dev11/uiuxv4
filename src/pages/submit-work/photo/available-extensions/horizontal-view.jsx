import React from 'react';
import MountainsIcon from '../../../../components/assets/svgs/mountains-icon'
import GifIcon from '../../../../components/assets/svgs/gif-icon'
import VideoIcon from '../../../../components/assets/svgs/video-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            flexWrap: 'wrap',
            width: '95%',
            margin:'auto',
            marginBottom:theme.spacing(2)
        },
        item: {
            background: '#c4c4c4',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 8,
            color: '#ffffff',
            width: '30%',
            borderRadius: 10,
            padding: 8,
            '&[data-active="false"]': {
                backgroundColor: '#fae6eb',
                color: '#ce0f3d'
            }
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            width: '100%'
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 12,
            textAlign: 'center',
            color: 'inherit'
        },
        subtitle: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 12,
            textAlign: 'center',
            color: 'inherit'
        },
        divider: {
            borderBottom: '2px solid #ffffff',
            marginTop: 4,
            marginBottom: 4,
            width: '100%',
            '&[data-active="false"]': {
                borderBottom: '2px solid #ce0f3d'
            }
        }
    })
);

function HorizontalView() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.item}>
                <div className={classes.row}>
                    <MountainsIcon />
                    <div className={classes.title}>Upload<br />Image</div>
                </div>
                <div className={classes.divider}></div>
                <div className={classes.subtitle}>JPG,PNG,GIF</div>
            </div>
            <div className={classes.item}>
                <div className={classes.row}>
                    <GifIcon />
                    <div className={classes.title}>Upload<br />Animation</div>
                </div>
                <div className={classes.divider}></div>
                <div className={classes.subtitle}>GIF</div>
            </div>
            <div data-active={false} className={classes.item}>
                <div className={classes.row}>
                    <VideoIcon />
                    <div className={classes.title}>Upload<br />Video</div>
                </div>
                <div data-active={false} className={classes.divider}></div>
                <div className={classes.subtitle}>MP4,AVI</div>
            </div>
        </div>
    )
}

export default HorizontalView