import React from 'react';
import MountainsIcon from '../../../../components/assets/svgs/mountains-icon'
import GifIcon from '../../../../components/assets/svgs/gif-icon'
import VideoIcon from '../../../../components/assets/svgs/video-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column'
        },
        item: {
            background: '#c4c4c4',
            borderRadius: 20,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 8,
            marginBottom: 8,
            maxWidth: 112,
            minHeight: 209,
            color: '#ffffff',
            '&[data-active="false"]': {
                backgroundColor: '#fae6eb',
                color: '#ce0f3d'
            }
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            width: '100%'
        },
        title: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 16,
            textAlign: 'center',
            color: 'inherit'
        },
        subtitle: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 16,
            textAlign: 'center',
            color: 'inherit'
        },
        divider: {
            borderBottom: '2px solid #ffffff',
            marginTop: 8,
            marginBottom: 8,
            width: '100%',
            '&[data-active="false"]': {
                borderBottom: '2px solid #ce0f3d'
            }
        }
    })
);

function VerticalView() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.item}>
                <MountainsIcon />
                <div className={classes.title}>Upload<br />Image</div>
                <div className={classes.divider}></div>
                <div className={classes.subtitle}>JPG<br />PNG<br />GIF</div>
            </div>
            <div className={classes.item}>
                <GifIcon />
                <div className={classes.title}>Upload<br />Animation</div>
                <div className={classes.divider}></div>
                <div className={classes.subtitle}>GIF</div>
            </div>
            <div data-active={false} className={classes.item}>
                <VideoIcon />
                <div className={classes.title}>Upload<br />Video</div>
                <div data-active={false} className={classes.divider}></div>
                <div className={classes.subtitle}>MP4<br />AVI</div>
            </div>
        </div>
    )
}

export default VerticalView