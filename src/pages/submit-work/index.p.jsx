import React from 'react'
import { Formik } from 'formik'
import { Grid } from '@material-ui/core';
import Form from './form'
import Photo from './photo'
import SubmitWorkLayout from '../../components/layouts/submit-work'
import PrivateRoute from '../../components/hocs/private-route'

const initialValues = {
  title: '',
  description: '',
  creativeFields: [],
  tools: [],
  tags: [],
  photo: {
    selected: null,
    images: []
  }
}

const SubmitWork = () => {

  const handleSubmit = values => {
    console.log(values)
  }

  return (
    <SubmitWorkLayout>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
      >
        {formikProps => (
          <Grid container>
            <Grid item xs={12} sm={12} md={8} lg={8} xl={8}>
              <Photo
                value={formikProps.values.photo}
                onChange={dta => {
                  formikProps.setFieldValue('photo', dta)
                }} />
            </Grid>
            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
              <Form {...formikProps} />
            </Grid>
          </Grid>
        )}
      </Formik>
    </SubmitWorkLayout>
  )
}

export default PrivateRoute(SubmitWork)
