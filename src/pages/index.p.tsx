import router from 'next/router'
import { useEffect } from 'react'

const Index = () => {
  useEffect(() => {
    router.replace('/home')
  }, [])

  return null
}

export default Index