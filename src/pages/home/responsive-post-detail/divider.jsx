import React from 'react';
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        borderBottom: '1px solid #c8c8c8'
    }
}));

function Divider() {
    const classes = useStyles()
    return (
        <div className={classes.root}></div>
    );
}

export default Divider;