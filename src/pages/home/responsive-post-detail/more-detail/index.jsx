import React, { useState } from 'react';
import { makeStyles, withStyles } from "@material-ui/core";
import Tools from './tools'
import CarouselNav from './carousel-nav'
import Divider from '../divider';
import MuiAccordion from '@material-ui/core/Accordion';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing(1),
        margin: theme.spacing(0.5),
    },
    title: {
        margin: theme.spacing(1),
        fontStyle: 'normal',
        fontWeight: '300',
        fontSize: 16
    },
    button: {
        cursor:'pointer',
        textAlign: 'center',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 12,
        color: theme.palette.primary.main
    }
}));


const Accordion = withStyles({
    root: {
        boxShadow: 'none',
        '&:before': {
            display: 'none',
        },
    },
    expanded: {},
})(MuiAccordion);

function MoreDetail({ images }) {
    const [expanded, setExpanded] = useState(false);

    const handleChange = () => {
        setExpanded(odt => !odt);
    };

    const classes = useStyles()
    return (
        <div className={classes.root}>

            <Accordion expanded={expanded}>
                <div className={classes.title}>
                    Just a few clicks to order fresh pastries, Cakes, and many other delicious desserts
                </div>
                <Divider />
                <Tools />
                <CarouselNav data={images} />
            </Accordion>
            <div
                onClick={handleChange}
                className={classes.button}>
                {expanded ? 'CLOSE DETAIL' : 'MORE DETAIL'}
            </div>
        </div>
    );
}

export default MoreDetail