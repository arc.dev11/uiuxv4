import React from 'react';
import { makeStyles } from "@material-ui/core";
import AdobeXdIcon from '../../../../components/assets/svgs/adobe-xd-icon'
import FigmaIcon from '../../../../components/assets/svgs/figma-icon'


const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: 'column'
    },
    toolsWrapper: {
        display: "flex",
        flexDirection: 'row',
        gap:theme.spacing(1)
    },
    title: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
        margin: theme.spacing(1),
    }
}));


function Tools() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <span className={classes.title}>Tools</span>
            <div className={classes.toolsWrapper}>
                <AdobeXdIcon width={60} height={60} />
                <FigmaIcon width={60} height={60} />
            </div>
        </div>
    );
}

export default Tools