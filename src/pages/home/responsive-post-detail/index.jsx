import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import router from 'next/router'
import Photo from './photo'
import Options from './options'
import Header from './header'
import Comments from './comments'
import MoreDetail from './more-detail'
import Tags from './tags'
import Divider from './divider'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  }
}));


const PostDetail = ({ id, data }) => {

  const classes = useStyles();

  const handleClose = () => {
    router.replace('/home')
  }


  return (
    <Dialog
      fullScreen
      open={true}
      onClose={handleClose}
    >
      <div className={classes.root}>
        <Header />
        <Photo {...data[id]} />
        <Options {...data[id]} />
        <MoreDetail {...data[id]}/>
        <Divider />
        <Tags />
        <Divider />
        <Comments />
      </div>
    </Dialog>
  );
}

export default PostDetail
