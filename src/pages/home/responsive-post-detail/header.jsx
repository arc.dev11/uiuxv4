import React from 'react';
import { Avatar } from '@material-ui/core'
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(1),
        paddingLeft: theme.spacing(3),
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center'
    },
    textWrapper: {
        padding: theme.spacing(1)
    },
    title: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
        color: theme.palette.primary.main
    },
    subtitle: {
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 16
    }
}));


function Header() {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Avatar>H</Avatar>
            <div className={classes.textWrapper}>
                <div className={classes.title}>Jane Cooper</div>
                <div className={classes.subtitle}>Jane Cooper</div>
            </div>
        </div>
    );
}

export default Header