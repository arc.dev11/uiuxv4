import React from "react";
import { makeStyles, Theme } from "@material-ui/core";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import CommentIcon from "@material-ui/icons/Comment";
import BookmarkIcon from "@material-ui/icons/Bookmark";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    "& svg": {
      color: "#707070",
      margin: theme.spacing(1),
      cursor: "pointer",
    },
  },
  left: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    fontStyle: "normal",
    fontWeight: 300,
    fontSize: 16,
    textAlign: "center",
    color: "#707070",
  },
  right: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    fontStyle: "normal",
    fontWeight: 300,
    fontSize: 16,
    textAlign: "center",
    color: "#707070",
  },
}));

const FadeSliderWithNav = ({ likes, comments }: any) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.left}>
        <ThumbUpIcon />
        {likes}
        <CommentIcon />
        {comments.length}
      </div>
      <div className={classes.right}>
        Save <BookmarkIcon />
      </div>
    </div>
  );
};

export default FadeSliderWithNav;
