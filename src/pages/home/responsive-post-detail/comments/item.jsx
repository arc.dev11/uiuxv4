import React from 'react';
import { Avatar } from '@material-ui/core'
import { makeStyles } from "@material-ui/core";
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ReplyIcon from '@material-ui/icons/Reply';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(1),
        display: "flex",
        flexDirection: "column"
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    headerLeftSide: {
        display: 'inline-flex',
        justifyContent: 'center'
    },
    title: {
        padding: theme.spacing(1),
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 16
    },
    createdAt: {
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 16,
        color: '#C8C8C8'
    },
    content: {
        paddingLeft: theme.spacing(5),
    },
    footer: {
        padding: theme.spacing(1),
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& svg': {
            color: '#707070',
            margin: theme.spacing(1),
            cursor: 'pointer'
        }
    },
    footerSides: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 16,
        textAlign: 'center',
        color: '#707070'
    }
}));

function Item({ title, subtitle, createdAt, mention, like }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.header}>
                <div className={classes.headerLeftSide}>
                    <Avatar>H</Avatar>
                    <span className={classes.title}>{title}</span>
                </div>
                <span className={classes.createdAt}>{createdAt}</span>
            </div>
            <div className={classes.content}>{subtitle}</div>
            <div className={classes.footer}>
                <div className={classes.footerSides}>
                    <ReplyIcon />{mention}
                </div>
                <div className={classes.footerSides}>
                    <ThumbUpIcon />{like}
                </div>
            </div>
        </div>
    );
}

export default Item