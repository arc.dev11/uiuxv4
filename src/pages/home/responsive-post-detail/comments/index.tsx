import React from "react";
import Comment from "./item";

const Index = () => {
  return (
    <div>
      <Comment
        mention={123}
        like={123}
        createdAt="8 days ago"
        title="Jane Cooper"
        subtitle="Just a few clicks to order fresh pastries, Cakes, and many other delicious desserts"
      />
      <Comment
        mention={12}
        like={32}
        createdAt="8 days ago"
        title="Fionna Green"
        subtitle="Just a few clicks to order fresh pastries, Cakes, and many other delicious desserts"
      />
    </div>
  );
};

export default Index;
