import { makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  dotsWrapper: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
    bottom: 20,
    position: "absolute",
  },
  dot: {
    width: theme.spacing(1),
    height: theme.spacing(1),
    background: "gray",
    borderRadius: "50%",
    margin: "0 3px",
    cursor: "pointer",
  },
  activeDot: {
    width: theme.spacing(1),
    height: theme.spacing(1),
    borderRadius: "50%",
    margin: "0 3px",
    background: "#3f3f3f",
    cursor: "default",
  },
}));

export default function DotsNav(props: any) {
  const classes = useStyles();
  const { data, onChange, activeSlide } = props;

  return (
    <div className={classes.dotsWrapper}>
      {React.Children.toArray(
        data.map((el: any, index: any) => {
          return (
            <div
              onClick={() => {
                if (onChange) onChange(index);
              }}
              className={
                activeSlide === index ? classes.activeDot : classes.dot
              }
            />
          );
        })
      )}
    </div>
  );
}
