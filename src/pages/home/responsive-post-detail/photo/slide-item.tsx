import { makeStyles } from "@material-ui/core";
import Image from "next/image";

const useStyles = makeStyles(() => ({
  root: {
    opacity: 0,
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    transition: "all 1s ease-in-out",
    '&[data-active="true"]': {
      opacity: 1,
    },
  },
}));

interface PropsType {
  background: string;
  alt: string;
  active: any;
}

const SlideItem = ({ background, active }: PropsType) => {
  const classes = useStyles();

  return (
    <div className={classes.root} data-active={active}>
      <Image layout="fill" src={background} alt="" />
    </div>
  );
};

export default SlideItem;
