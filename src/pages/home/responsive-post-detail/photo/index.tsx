import React, { useState } from "react";
import { makeStyles } from "@material-ui/core";
import SlideItem from "./slide-item";
import DotsNav from './dots-nav'

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    position: "relative",
    width: "100%",
    paddingTop: "65%",
  },
}));

const FadeSlider = ({ images }: any) => {
  const classes = useStyles();
  const [activeSlide, setActiveSlide] = useState(0);

  return (
    <div className={classes.root}>
      {React.Children.toArray(
        images.map((slide: any, index: any) => {
          return (
            <SlideItem
              background={slide.src}
              alt={slide?.alt}
              active={index === activeSlide}
            />
          );
        })
      )}
      <DotsNav
        data={images}
        activeSlide={activeSlide}
        onChange={(v: any) => {
          setActiveSlide(() => v);
        }}
      />
    </div>
  );
};

export default FadeSlider;
