import React from 'react';
import Right from './right'
import Left from './left'
import ResponsiveRight from './responsive-right'
import ResponsiveLeft from './responsive-left'
import { makeStyles } from '@material-ui/core/styles';
import { Formik } from 'formik'
import { Hidden } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%'
    }
}));

const initialValues = {
    sortBy: 'popular',
    category: '',
    madeWith: '',
    tag: ''
}

function Filter(props) {
    const classes = useStyles()

    const handleSubmit = values => {
        console.log(values)
    }

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={handleSubmit}
        >
            {({
                values,
                setFieldValue,
                handleSubmit
            }) => (
                <form
                    className={classes.root}
                    onSubmit={handleSubmit} >
                    <Hidden smDown>
                        <Left values={values} setFieldValue={setFieldValue} />
                        <Right values={values} setFieldValue={setFieldValue} />
                    </Hidden>
                    <Hidden mdUp>
                        <ResponsiveLeft values={values} setFieldValue={setFieldValue} />
                        <ResponsiveRight values={values} setFieldValue={setFieldValue} />
                    </Hidden>
                </form>
            )}
        </Formik>

    );
}

export default Filter;