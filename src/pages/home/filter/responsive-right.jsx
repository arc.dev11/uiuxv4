import React, { useState, useRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import dynamic from 'next/dynamic'
import SortIcon from '@material-ui/icons/Sort';

const SingleSelect = dynamic(
    () => import('../../../components/formik-custom-fields/single-select'),
    { ssr: false }
)

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        position: 'relative'
    },
    button: {
        cursor: 'pointer',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
        color: '#CE0F3D',
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        '& svg': {
            margin: theme.spacing(1)
        }
    },
    list: {
        padding: theme.spacing(5),
        width: '100vw',
        right: 0,
        zIndex: 2,
        position: 'absolute',
        background: '#FFFFFF',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '0px 0px 10px'
    },
    label: {
        padding: theme.spacing(1),
        display: 'block'
    }
}));


function ResponsiveRight({ values, setFieldValue }) {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const wrapper = useRef()
    
    useEffect(() => {
        document.addEventListener("mousedown", handleDocClick, false);
        return () => {
            document.removeEventListener("mousedown", handleDocClick, false);
        }
    }, [])

    const handleButtonClick = () => {
        setOpen(true)
    }

    const handleDocClick = e => {
        if (!wrapper.current.contains(e.target)) {
            setOpen(false)
        }
    };

    return (
        <div ref={wrapper} className={classes.root}>
            <div onClick={handleButtonClick} className={classes.button}>
                <span>Filter</span>
                <SortIcon />
            </div>
            <div style={{ display: open ? 'block' : 'none' }} className={classes.list}>
                <label className={classes.label}>Category</label>
                <SingleSelect
                    uniqueKey={'value'}
                    data={[
                        {
                            label: "One",
                            value: 1
                        },
                        {
                            label: "Two",
                            value: 2
                        },
                        {
                            label: "Three",
                            value: 3
                        }
                    ]}
                    selected={values.category}
                    toggleChangeListItem={dta => {
                        setFieldValue('category', dta)
                    }}
                    removeChangeListItem={() => {
                        setFieldValue('category', null)
                    }}
                />
                <label className={classes.label}>Made With</label>
                <SingleSelect
                    uniqueKey={'value'}
                    data={[
                        {
                            label: "One",
                            value: 1
                        },
                        {
                            label: "Two",
                            value: 2
                        },
                        {
                            label: "Three",
                            value: 3
                        }
                    ]}
                    selected={values.madeWith}
                    toggleChangeListItem={dta => {
                        setFieldValue('madeWith', dta)
                    }}
                    removeChangeListItem={() => {
                        setFieldValue('madeWith', null)
                    }}
                />
                <label className={classes.label}>Tag</label>
                <SingleSelect
                    uniqueKey={'value'}
                    data={[
                        {
                            label: "One",
                            value: 1
                        },
                        {
                            label: "Two",
                            value: 2
                        },
                        {
                            label: "Three",
                            value: 3
                        }
                    ]}
                    selected={values.tag}
                    toggleChangeListItem={dta => {
                        setFieldValue('tag', dta)
                    }}
                    removeChangeListItem={() => {
                        setFieldValue('tag', null)
                    }}
                />
            </div>
        </div>
    );
}

export default ResponsiveRight