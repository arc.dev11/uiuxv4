import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        margin: 0,
        padding: 0,
        listStyle: 'none',
        gap: theme.spacing(2),
        borderRight: '1px solid #c8c8c8'
    },
    item: {
        padding:theme.spacing(1),
        margin: 'auto',
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 16,
        color: '#707070',
        '&[data-active="true"]': {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18,
            color: '#CE0F3D'
        },
    }
}));

function Left(props) {
    const classes = useStyles()
    return (
        <ul className={classes.root}>
            <li data-active={true} className={classes.item}>Popular</li>
            <li className={classes.item}>Recent</li>
            <li className={classes.item}>Instagram</li>
        </ul>
    );
}

export default Left