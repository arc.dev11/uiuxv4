import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import dynamic from 'next/dynamic'

const SingleSelect = dynamic(
    () => import('../../../components/formik-custom-fields/single-select'),
    { ssr: false }
)

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    select: {
        maxWidth: 200
    },
    label: {
        padding: theme.spacing(1)
    }
}));


function Right({ values, setFieldValue }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <label className={classes.label}>Category</label>
            <SingleSelect
                classes={{ root: classes.select }}
                uniqueKey={'value'}
                data={[
                    {
                        label: "One",
                        value: 1
                    },
                    {
                        label: "Two",
                        value: 2
                    },
                    {
                        label: "Three",
                        value: 3
                    }
                ]}
                selected={values.category}
                toggleChangeListItem={dta => {
                    setFieldValue('category', dta)
                }}
                removeChangeListItem={() => {
                    setFieldValue('category', null)
                }}
            />
            <label className={classes.label}>Made With</label>
            <SingleSelect
                classes={{ root: classes.select }}
                uniqueKey={'value'}
                data={[
                    {
                        label: "One",
                        value: 1
                    },
                    {
                        label: "Two",
                        value: 2
                    },
                    {
                        label: "Three",
                        value: 3
                    }
                ]}
                selected={values.madeWith}
                toggleChangeListItem={dta => {
                    setFieldValue('madeWith', dta)
                }}
                removeChangeListItem={() => {
                    setFieldValue('madeWith', null)
                }}
            />
            <label className={classes.label}>Tag</label>
            <SingleSelect
                classes={{ root: classes.select }}
                uniqueKey={'value'}
                data={[
                    {
                        label: "One",
                        value: 1
                    },
                    {
                        label: "Two",
                        value: 2
                    },
                    {
                        label: "Three",
                        value: 3
                    }
                ]}
                selected={values.tag}
                toggleChangeListItem={dta => {
                    setFieldValue('tag', dta)
                }}
                removeChangeListItem={() => {
                    setFieldValue('tag', null)
                }}
            />
        </div>
    );
}

export default Right