import React, { useState, useRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        position: 'relative'
    },
    button: {
        cursor: 'pointer',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
        color: '#CE0F3D',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        '& svg': {
            margin: theme.spacing(1)
        }
    },
    list: {
        width: '80vw',
        left: '10vw',
        zIndex: 2,
        position: 'absolute',
        display: 'flex',
        flexDirection: 'column',
        margin: 0,
        padding: 0,
        listStyle: 'none',
        background: '#FFFFFF',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '0px 0px 10px'
    },
    item: {
        padding: theme.spacing(2),
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 16,
        color: '#707070',
        '&[data-active="true"]': {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18,
            color: '#CE0F3D'
        },
    }
}));

function ResponsiveLeft(props) {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const wrapper = useRef()

    useEffect(() => {
        document.addEventListener("mousedown", handleDocClick, false);
        return () => {
            document.removeEventListener("mousedown", handleDocClick, false);
        }
    }, [])

    const handleButtonClick = () => {
        setOpen(true)
    }

    const handleDocClick = e => {
        if (!wrapper.current.contains(e.target)) {
            setOpen(false)
        }
    };

    return (
        <div ref={wrapper} className={classes.root}>
            <div onClick={handleButtonClick} className={classes.button}>
                <span>Popular</span>
                <ExpandMoreIcon />
            </div>
            <ul style={{ display: open ? 'block' : 'none' }} className={classes.list}>
                <li data-active={true} className={classes.item}>Popular</li>
                <li className={classes.item}>Recent</li>
                <li className={classes.item}>Instagram</li>
            </ul>
        </div>
    );
}

export default ResponsiveLeft