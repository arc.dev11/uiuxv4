import React, { useState, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Image from "next/image";

const useStyles = makeStyles((theme) => ({
  imageItem: {
    position: "relative",
    display: "inline-block",
    cursor: "pointer",
    width: 173,
    height: "100%",
    margin: theme.spacing(0.5),
  },
  container: {
    height: "100%",
    cursor: "pointer",
    userSelect: "none",
    overflowY: "hidden",
    overflowX: "scroll",
    whiteSpace: "nowrap",
  },
}));

export default function CarouselNav(props: any) {
  const classes = useStyles();
  const { data, onChange } = props;

  const tagsDivRef: any = useRef(null);
  const [isDown, setIsDown] = useState(false);
  const [startX, setStartX] = useState(0);
  const [scrollLeft, setScrollLeft] = useState(0);

  return (
    <div
      ref={tagsDivRef}
      onMouseMove={(e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - tagsDivRef.current.offsetLeft;
        const walk = x - startX;
        tagsDivRef.current.scrollLeft = scrollLeft - walk;
      }}
      onMouseDown={(e) => {
        setIsDown(true);
        setStartX(e.pageX - tagsDivRef.current.offsetLeft);
        setScrollLeft(tagsDivRef.current.scrollLeft);
      }}
      onMouseLeave={() => {
        setIsDown(false);
      }}
      onMouseUp={() => {
        setIsDown(false);
      }}
      className={clsx(classes.container, "no-bar-overflow")}
    >
      {React.Children.toArray(
        data.map((el: any, index: any) => {
          // let slideStyle = { backgroundImage: `url( ${el.background})` };
          return (
            <div
              className={classes.imageItem}
              onClick={() => {
                if (onChange) onChange(index);
              }}
            >
              <Image alt="" layout="fill" src={el.src} />
            </div>
          );
        })
      )}
    </div>
  );
}
