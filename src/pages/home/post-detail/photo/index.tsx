import React, { useState } from "react";
import { makeStyles, Theme } from "@material-ui/core";
import SlideItem from "./slide-item";
import CarouselNav from "./carousel-nav";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import CommentIcon from "@material-ui/icons/Comment";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import DotsNav from "./dots-nav";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
  },
  sliderWrapper: {
    position: "relative",
    width: "100%",
    paddingTop: "65%",
  },
  navWrapper: {
    minHeight: 140,
  },
  footer: {
    padding: theme.spacing(1),
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    "& svg": {
      color: "#707070",
      margin: theme.spacing(1),
      cursor: "pointer",
    },
  },
  footerLeft: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    fontStyle: "normal",
    fontWeight: 300,
    fontSize: 16,
    textAlign: "center",
    color: "#707070",
  },
  footerRight: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    fontStyle: "normal",
    fontWeight: 300,
    fontSize: 16,
    textAlign: "center",
    color: "#707070",
  },
  carouselTitle: {
    padding: theme.spacing(1),
    fontStyle: "normal",
    fontWeight: 300,
    fontSize: 16
  },
}));

const FadeSliderWithNav = ({ images, likes, comments }: any) => {
  const classes = useStyles();
  const [activeSlide, setActiveSlide] = useState(0);

  return (
    <div className={classes.root}>
      <div className={classes.sliderWrapper}>
        {React.Children.toArray(
          images.map((slide: any, index: any) => {
            return (
              <SlideItem
                background={slide.src}
                alt={slide?.alt}
                active={index === activeSlide}
              />
            );
          })
        )}
      </div>
      <div className={classes.footer}>
        <div className={classes.footerLeft}>
          <ThumbUpIcon />
          {likes}
          <CommentIcon />
          {comments.length}
        </div>
        <DotsNav
          data={images}
          activeSlide={activeSlide}
          onChange={(v: any) => {
            setActiveSlide(() => v);
          }}
        />
        <div className={classes.footerRight}>
          Save <BookmarkIcon />
        </div>
      </div>
      <div className={classes.carouselTitle}>More From Jane Cooper</div>
      <div className={classes.navWrapper}>
        <CarouselNav
          data={images}
          onChange={(v: any) => {
            setActiveSlide(() => v);
          }}
        />
      </div>
    </div>
  );
};

export default FadeSliderWithNav;
