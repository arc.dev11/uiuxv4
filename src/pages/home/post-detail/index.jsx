import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import router from 'next/router'
import Photo from './photo'
import Info from './info'

const useStyles = makeStyles((theme) => ({
  dialog: {
    width: '100%',
    padding:theme.spacing(1)
  },
  root: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: '100%'
  }
}));


const PostDetail = ({ id, data }) => {

  const classes = useStyles();

  const handleClose = () => {
    router.replace('/home')
  }


  return (
    <Dialog
      classes={{
        paper: classes.dialog
      }}
      maxWidth="lg"
      open={true}
      onClose={handleClose}
    >
      <div className={classes.root}>
        <Photo {...data[id]} />
        <Info />
      </div>
    </Dialog>
  );
}

export default PostDetail
