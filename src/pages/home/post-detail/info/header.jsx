import React from 'react';
import { Avatar } from '@material-ui/core'
import { makeStyles } from "@material-ui/core";
import VerifiedUserIcon from '../../../../components/assets/svgs/verified-user-icon'
import ProIcon from '../../../../components/assets/svgs/pro-icon'

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(1),
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
        '& span': {
            paddingLeft: 4,
            paddingRight: 4
        }
    },
    title: {
        padding: theme.spacing(1),
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18
    }
}));


function Header(props) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Avatar>H</Avatar>
            <span className={classes.title}>Jane Cooper</span>
            <span>
                <ProIcon color="#707070" />
                <VerifiedUserIcon color="#707070" />
            </span>
        </div>
    );
}

export default Header