import React from 'react';
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: theme.spacing(0.5),
    },
    tag: {
        padding: theme.spacing(0.5),
        marginTop: theme.spacing(0.2),
        marginBottom: theme.spacing(0.2),
        marginRight: theme.spacing(1),
        background: '#F3F3F3',
        borderRadius: 15,
        fontStyle: 'normal',
        fontWeight: 300,
        fontSize: 16,
        color: '#CE0F3D'
    }
}));

const data = ['graphic design',
    'graphic', 'adobe xd', 'figma',
    'uiux', 'web design', 'android',
    'ui design', 'ux design',
    'web design', 'app design']

function Tags(props) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            {data.map((el, index) => {
                return (
                    <div key={index} className={classes.tag}>{el}</div>
                )
            })}
        </div>
    );
}

export default Tags