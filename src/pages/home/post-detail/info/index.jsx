import React from 'react';
import { makeStyles } from "@material-ui/core";
import Tools from './tools'
import Tags from './tags'
import Header from './header'
import Divider from './divider'
import Comments from './comments'

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        maxWidth: '50%'
    },
    title: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
        color: '#CE0F3D'
    },
    subtitle: {
        fontStyle: 'normal',
        fontWeight: '300',
        fontSize: 16
    },
    content: {
        padding: theme.spacing(1)
    }
}));

function Info(props) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Tools />
            <div className={classes.content}>
                <Header />
                <div className={classes.title}>
                    Text example style
                </div>
                <div className={classes.subtitle}>
                    Just a few clicks to order fresh pastries, Cakes, and many other delicious desserts
                </div>
                <Divider />
                <Tags />
                <Divider />
                <Comments />
            </div>
        </div>
    );
}

export default Info;