import React from 'react';
import { makeStyles } from "@material-ui/core";
import AdobeXdIcon from '../../../../components/assets/svgs/adobe-xd-icon'
import FigmaIcon from '../../../../components/assets/svgs/figma-icon'


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(1),
        display: "flex",
        flexDirection: 'column'
    },
    title: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        color: '#000000',
        marginBottom:theme.spacing(1),
    }
}));


function Info(props) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <span className={classes.title}>Tools</span>
            <AdobeXdIcon width={60} height={60} />
            <FigmaIcon width={60} height={60} />
        </div>
    );
}

export default Info;