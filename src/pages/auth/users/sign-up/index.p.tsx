import router from 'next/router'
import { useEffect } from 'react'

const Index = () => {
  useEffect(() => {
    router.replace('/auth/users/sign-up/step-one')
  }, [])

  return null
}

export default Index