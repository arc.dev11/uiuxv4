import React from "react";

const DropDrop = (props: any) => {
  const onDrop = (e: any) => {
    e.preventDefault();
    let files = mapFileListToArray(e.dataTransfer.files);
    props.onFilesDropped(files);
  };

  const onDragOver = (e: any) => {
    e.preventDefault();
  };

  const mapFileListToArray = (fileList: any) => {
    let fl = fileList.length;
    let files = [];
    for (let i = 0; i < fl; i++) {
      files.push(fileList[i]);
    }
    return files;
  };

  return (
    <div
      className="RuiSubmitWorkEditor-dragDrop"
      onDrop={onDrop}
      onDragOver={onDragOver}
    />
  );
};

export default DropDrop;
