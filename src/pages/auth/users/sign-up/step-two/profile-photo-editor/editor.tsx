import React, { useState, useCallback } from "react";
import Cropper from "react-easy-crop";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      flexGrow: 1,
      position: "relative",
      borderRadius: 20,
    },
  })
);

const Editor = ({ src, onChange }: any) => {
  const classes = useStyles();
  const [crop, setCrop]: any = useState({ x: 0, y: 0 });
  const [zoom, setZoom]: any = useState(1);

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    onChange(croppedAreaPixels);
  }, []);

  return (
    <div className={classes.root}>
      <Cropper
        image={src}
        crop={crop}
        zoom={zoom}
        aspect={4 / 3}
        onCropChange={setCrop}
        onCropComplete={onCropComplete}
        onZoomChange={setZoom}
      />
    </div>
  );
};

export default Editor;
