import React from "react";
import UploadPlaceHolderImage from "../../../../../../components/assets/svgs/upload-placeholder-image";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import theme from "../../../../../../theme";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      borderRadius: 20,
    },
    dragdrop: {
      marginLeft: theme.spacing(5),
      marginRight: theme.spacing(5),
      padding: theme.spacing(3),
      backgroundColor: theme.palette.secondary.main,
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    title: {
      textAlign: "center",
      padding: theme.spacing(3),
      color: theme.palette.primary.main,
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: 18,
    },
    buttonsWrapper: {
      padding: theme.spacing(3),
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      gap: 12,
    },
    subtitle: {
      padding: theme.spacing(1),
      fontSize: 16,
      textAlign: "center",
      fontWeight: "bold",
    },
    icon: {},
  })
);

const PlaceHolder = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.title}>Crop Your Image</div>
      <div className={classes.dragdrop}>
        <UploadPlaceHolderImage
          className={classes.icon}
          color={theme.palette.primary.main}
        />
        <div className={classes.subtitle}>Drag And Drop Your Image</div>
        <div className={classes.subtitle}>
          or click on Browse and select your image
        </div>
      </div>
    </div>
  );
};

export default PlaceHolder;
