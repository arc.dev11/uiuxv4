import React, { useState, useEffect, useRef } from "react";
import Dialog from "@material-ui/core/Dialog";
import PlaceHolder from "./placeholder";
import getCroppedImg from "./croper";
import Editor from "./editor";
import DragDrop from "./dragdrop";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 556,
      maxHeight: 607,
      width: "80vw",
      height: "80vh",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    buttonsWrapper: {
      padding: theme.spacing(3),
      display: "flex",
      flexDirection: "row",
      justifyContent: "center",
      gap: 12,
    },
  })
);

export interface ProfilePhotoEditorProps {
  open: boolean;
  onClose: (value: string) => void;
}

const ProfilePhotoEditor = (props: ProfilePhotoEditorProps) => {
  const { onClose, open, value, onChange }: any = props;

  const classes = useStyles();
  const [state, setState] = useState(value);
  const fileInput: any = useRef();
  const vars = useRef({
    croppedAreaPixels: {},
  }).current;

  useEffect(() => {
    onChange(state);
  }, [state]);

  const handleChange = (e: any) => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();
      reader.onload = function (ev: any) {
        setState(ev.target.result);
      }.bind(this);
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onFilesDropped = (files: any) => {
    let reader = new FileReader();
    reader.onload = function (ev: any) {
      setState(ev.target.result);
    };
    reader.readAsDataURL(files[0]);
  };

  const handleCropperChange = (croppedAreaPixels: any) => {
    vars.croppedAreaPixels = croppedAreaPixels;
  };

  const handleDone = async () => {
    try {
      const croppedImage = await getCroppedImg(state, vars.croppedAreaPixels);
      setState(croppedImage);
      onClose();
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <input
        style={{ display: "none" }}
        ref={fileInput}
        type="file"
        onChange={handleChange}
      />

      <div className={classes.root}>
        {state ? (
          <Editor src={state} onChange={handleCropperChange} />
        ) : (
          <div>
            <PlaceHolder />
            <DragDrop onFilesDropped={onFilesDropped} />
          </div>
        )}

        <div className={classes.buttonsWrapper}>
          <Button
            onClick={() => fileInput.current.click()}
            color="secondary"
            variant="contained"
          >
            Browse
          </Button>
          <Button onClick={handleDone} color="primary" variant="contained">
            done
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

export default ProfilePhotoEditor;
