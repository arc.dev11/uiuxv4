import React, { useState } from 'react'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      gap: 8,

    },
    item: {
      cursor: 'pointer',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: '#f3f3f3',
      borderRadius: 28,
      width: 225,
      height: 56,
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      color: '#ce0f3d',
      '&[data-active="true"]': {
        backgroundColor: '#ce0f3d',
        color: '#ffffff'
      }
    }
  })
);

const UserTypeField = () => {

  const classes = useStyles()
  const [selected, setSelected] = useState('employee')

  const handleClickItem = (v) => {
    setSelected(() => v)
  }

  return (
    <div className={classes.root}>
      <div
        data-active={selected === 'company'}
        onClick={() => handleClickItem('company')}
        className={classes.item}>
        Company
      </div>
      <div
        data-active={selected === 'employee'}
        onClick={() => handleClickItem('employee')}
        className={classes.item}>
        Employee
      </div>
    </div>
  )
}

export default UserTypeField
