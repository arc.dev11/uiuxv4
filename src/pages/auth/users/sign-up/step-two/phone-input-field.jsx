import React from 'react'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      background: '#f3f3f3',
      borderRadius: 28,
      width: 466,
      height: 56
    },
    input: {
      padding: 16,
      flexGrow: 1,
      height: '100%'
    },
    button: {
      cursor: 'pointer',
      marginLeft: 8,
      marginRight: 8,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: '#ce0f3d',
      borderRadius: 25,
      width: 133,
      height: 47,
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: 18,
      textAlign: 'center',
      color: '#ffffff'
    }
  })
);

const PhoneInputField = () => {

  const classes = useStyles()

  return (
    <div className={classes.root}>
      <input className={classes.input} type="text" />
      <div className={classes.button}>Verify</div>
    </div>
  )
}

export default PhoneInputField
