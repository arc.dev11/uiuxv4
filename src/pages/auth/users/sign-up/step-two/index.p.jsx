import React, { useState } from 'react'
import Link from 'next/link'
import AuthLayout from '../../../../../components/layouts/auth'
import { Formik } from 'formik'
import ProfileImage from '../../../../../components/assets/svgs/profile-image'
import UserTypeField from './user-type-field'
import PhoneInputField from './phone-input-field'
import ProfilePhotoEditor from './profile-photo-editor'
import dynamic from 'next/dynamic'
import { makeStyles } from "@material-ui/core";

const SingleSelect = dynamic(
  () => import('../../../../../components/formik-custom-fields/single-select'),
  { ssr: false }
)

const MultiSelect = dynamic(
  () => import('../../../../../components/formik-custom-fields/multi-select'),
  { ssr: false }
)

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    maxWidth: 466,
    margin: 'auto',
    height: '100%'
  },
  label: {
    display: 'block',
    padding: 10,
    paddingLeft: 30,
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: 18
  },
  input: {
    paddingRight: 20,
    paddingLeft: 20,
    background: '#f3f3f3',
    borderRadius: 28,
    height: 56,
    width: '100%',
    color: '#ce0f3d'
  },
  button: {
    marginTop: 28,
    cursor: 'pointer',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#ffffff',
    background: '#ce0f3d',
    borderRadius: 28,
    width: 466,
    height: 56
  },
  title: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
    padding: 8,
    '& a': {
      paddingLeft: 4,
      paddingLeft: 4,
      color: '#ce0f3d'
    }
  },
  checkboxWrapper: {
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 382,
    marginTop: 8,
    marginLeft: 'auto',
    marginRight: 'auto',
    '& input': {
      margin: 8
    }
  },
  checkboxLabel: {
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: 18,
    '& a': {
      color: '#ce0f3d'
    }
  },
  profileImage: {
    marginLeft: 'auto',
    marginRight: 'auto',
    cursor: 'pointer'
  },
  profileImageText: {
    marginBottom: 8,
    marginLeft: 'auto',
    marginRight: 'auto',
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: 16
  }
}));



const initialValues = {
  profileImage: null,
  country: '',
  city: '',
  field: [],
  skills: [],
  phoneNumber: '',
  agree: false
}

const StepTwo = () => {

  const classes = useStyles()
  const [photoEditorStatus, setPhotoEditorStatus] = useState(false)

  const handleSubmit = values => {
    console.log(values)
  }

  const onKeyDown = keyEvent => {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  return (
    <AuthLayout>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
      >
        {({
          errors,
          values,
          setFieldValue,
          handleSubmit,
          handleChange,
          handleBlur
        }) => (
          <form className={classes.root} onSubmit={handleSubmit} onKeyDown={onKeyDown}>

            <ProfileImage
              onClick={() => setPhotoEditorStatus(true)}
              className={classes.profileImage} />

            <ProfilePhotoEditor
              onClose={() => setPhotoEditorStatus(false)}
              open={photoEditorStatus}
              value={values.profileImage}
              onChange={dta => {
                setFieldValue('profileImage', dta)
              }} />

            <div className={classes.profileImageText}>Select your profile image</div>

            <label className={classes.label}>Country</label>
            <SingleSelect
              uniqueKey={'value'}
              data={[
                {
                  label: "One",
                  value: 1
                },
                {
                  label: "Two",
                  value: 2
                },
                {
                  label: "Three",
                  value: 3
                }
              ]}
              selected={values.country}
              toggleChangeListItem={dta => {
                setFieldValue('country', dta)
              }}
              removeChangeListItem={() => {
                setFieldValue('country', null)
              }}
            />

            <label className={classes.label}>City</label>
            <SingleSelect
              uniqueKey={'value'}
              data={[
                {
                  label: "One",
                  value: 1
                },
                {
                  label: "Two",
                  value: 2
                },
                {
                  label: "Three",
                  value: 3
                }
              ]}
              selected={values.city}
              toggleChangeListItem={dta => {
                setFieldValue('city', dta)
              }}
              removeChangeListItem={() => {
                setFieldValue('city', null)
              }}
            />

            <label className={classes.label}>I’m a?</label>
            <UserTypeField />


            <label className={classes.label}>Select Your Field</label>
            <MultiSelect
              uniqueKey={'value'}
              data={[
                {
                  label: "One",
                  value: 1
                },
                {
                  label: "Two",
                  value: 2
                },
                {
                  label: "Three",
                  value: 3
                }
              ]}
              selected={values.field}
              toggleChangeListItem={dta => {
                setFieldValue('field', Array.from(new Set([...values.field, dta])))
              }}
              removeChangeListItem={dta => {
                setFieldValue('field', values.field.filter(el => el !== dta))
              }}
            />
            {errors.field && <div>{errors.field}</div>}

            <label className={classes.label}>Skils</label>
            <MultiSelect
              uniqueKey={'value'}
              data={[
                {
                  label: "One",
                  value: 1
                },
                {
                  label: "Two",
                  value: 2
                },
                {
                  label: "Three",
                  value: 3
                }
              ]}
              selected={values.skills}
              toggleChangeListItem={dta => {
                setFieldValue('skills', Array.from(new Set([...values.skills, dta])))
              }}
              removeChangeListItem={dta => {
                setFieldValue('skills', values.skills.filter(el => el !== dta))
              }}
            />
            {errors.skills && <div>{errors.skills}</div>}

            <label className={classes.label}>Phone Number</label>
            <PhoneInputField />

            <div className={classes.checkboxWrapper}>

              <input
                id="agree"
                type="checkbox"
                onChange={handleChange}
                onBlur={handleBlur}
                checked={values.agree}
                name="agree" />

              <label
                className={classes.checkboxLabel}
                htmlFor="agree">Yes! Send me genuinely useful emails
                every now and then to help me get the most out of UiUx-CENTER
              </label>

            </div>

            <button
              className={classes.button}
              type="submit">Done</button>

            <div className={classes.title}>
              Already a member?
              <Link href="/auth/users/sign-in">
                <a>Sign In</a>
              </Link>
            </div>

          </form>
        )}
      </Formik>
    </AuthLayout>
  )
}

export default StepTwo
