import React from 'react'
import Link from 'next/link'
import AuthLayout from '../../../../components/layouts/auth'
import { Formik } from 'formik'
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    maxWidth: 466,
    margin: 'auto',
    height: '100%'
  },
  label: {
    display: 'block',
    padding: 10,
    paddingLeft: 30,
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: 18
  },
  input: {
    paddingRight: 20,
    paddingLeft: 20,
    background: '#f3f3f3',
    borderRadius: 28,
    height: 56,
    width: '100%',
    color: '#ce0f3d'
  },
  button: {
    marginTop: 28,
    cursor: 'pointer',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#ffffff',
    background: '#ce0f3d',
    borderRadius: 28,
    width: 466,
    height: 56
  },
  title: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
    padding: 8,
    '& a': {
      paddingLeft: 4,
      paddingLeft: 4,
      color: '#ce0f3d'
    }
  },
  checkboxWrapper: {
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 382,
    marginTop: 8,
    marginLeft: 'auto',
    marginRight: 'auto',
    '& input': {
      margin: 8
    }
  },
  checkboxLabel: {
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: 18,
    '& a': {
      color: '#ce0f3d'
    }
  },
}));


const initialValues = {
  name: '',
  username: '',
  email: '',
  password: '',
  agree: false
}

const StepOne = () => {

  const classes = useStyles()

  const handleSubmit = values => {
    console.log(values)
  }

  const onKeyDown = keyEvent => {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  return (
    <AuthLayout>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
      >
        {({
          errors,
          values,
          setFieldValue,
          handleSubmit,
          handleChange,
          handleBlur
        }) => (
          <form className={classes.root} onSubmit={handleSubmit} onKeyDown={onKeyDown}>

            <label className={classes.label}>Name</label>
            <input
              className={classes.input}
              type="text"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.name}
              name="name"
            />
            {errors.name && <div>{errors.name}</div>}

            <label className={classes.label}>Username</label>
            <input
              className={classes.input}
              type="text"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.username}
              name="username"
            />
            {errors.username && <div>{errors.username}</div>}


            <label className={classes.label}>Email</label>
            <input
              className={classes.input}
              type="text"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              name="email"
            />
            {errors.email && <div>{errors.email}</div>}

            <label className={classes.label}>Password</label>
            <input
              className={classes.input}
              type="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              name="password"
            />
            {errors.password && <div>{errors.password}</div>}



            <div className={classes.checkboxWrapper}>

              <input
                id="agree"
                type="checkbox"
                onChange={handleChange}
                onBlur={handleBlur}
                checked={values.agree}
                name="agree" />

              <label
                className={classes.checkboxLabel}
                htmlFor="agree">Creating an account means you’re okay with our
                &nbsp;<a>Terms of Service</a>&nbsp;Terms of Service,
                &nbsp;<a>Privacy Policy</a>&nbsp;, and our default
                &nbsp;<a>Notification Settings</a>&nbsp;.
              </label>

            </div>



            <button
              className={classes.button}
              type="submit">Create Account</button>

            <div className={classes.title}>
              Already a member?
              <Link href="/auth/users/sign-in">
                <a>Sign In</a>
              </Link>
            </div>

          </form>
        )}
      </Formik>
    </AuthLayout>
  )
}

export default StepOne
