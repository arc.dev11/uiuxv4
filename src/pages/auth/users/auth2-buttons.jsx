import React from 'react';
import { signIn } from "next-auth/client";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import GoogleIcon from '../../../components/assets/svgs/google-icon'

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: 'flex',
            flexDirection: 'column',
            marginBottom: theme.spacing(1)
        },
        google: {
            cursor: 'pointer',
            backgroundColor: '#4285F4',
            borderRadius: 28,
            width: '100%',
            height: 56,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            '& button': {
                cursor: 'pointer',
                flexGrow: 1,
                fontStyle: 'normal',
                fontWeight: 'bold',
                fontSize: 18,
                textAlign: 'center',
                color: '#FFFFFF'
            },
            '& span': {
                cursor: 'pointer',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                background: '#FFFFFF',
                borderRadius: '28px 0px 0px 28px',
                width: 80,
                height: 48,
                marginLeft: theme.spacing(0.5)
            }
        }
    })
);

function Auth2Buttons({ providers }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            {Object.values(providers).map((provider) => {
                if (provider.name === "Google") {
                    return (
                        <div
                            key={provider.id}
                            className={classes.google}
                            onClick={() => signIn(provider.id)}>
                            <GoogleIcon />
                            <button >
                                Login With Google
                            </button>
                        </div >
                    )
                }
            })}
        </div>
    );
}

export default Auth2Buttons