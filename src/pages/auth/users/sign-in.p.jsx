import React from "react";
import Link from "next/link";
import AuthLayout from "../../../components/layouts/auth";
import { Formik } from "formik";
import Auth2Buttons from './auth2-buttons'
import { providers, getSession } from "next-auth/client";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    maxWidth: 466,
    margin: 'auto',
    height: '100%'
  },
  label: {
    display: 'block',
    padding: 10,
    paddingLeft: 30,
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: 18
  },
  input: {
    paddingRight: 20,
    paddingLeft: 20,
    background: '#f3f3f3',
    borderRadius: 28,
    height: 56,
    width: '100%',
    color: '#ce0f3d'
  },
  button: {
    marginTop: 28,
    cursor: 'pointer',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#ffffff',
    background: '#ce0f3d',
    borderRadius: 28,
    width: 466,
    height: 56
  },
  title: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
    padding: 8,
    '& a': {
      paddingLeft: 4,
      paddingLeft: 4,
      color: '#ce0f3d'
    }
  }
}));


const initialValues = {
  email: "",
  password: "",
};

const SignIn = ({ providers }) => {
  const classes = useStyles()
  const handleSubmit = values => {
    console.log(values);
  };

  const onKeyDown = keyEvent => {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  };

  return (
    <AuthLayout>
      <Formik initialValues={initialValues} onSubmit={handleSubmit}>
        {({
          errors,
          values,
          setFieldValue,
          handleSubmit,
          handleChange,
          handleBlur,
        }) => (
          <form
            className={classes.root}
            onSubmit={handleSubmit}
            onKeyDown={onKeyDown}
          >
            <Auth2Buttons providers={providers} />

            <label className={classes.label}>Email</label>
            <input
              className={classes.input}
              type="text"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              name="email"
            />
            {errors.email && <div>{errors.email}</div>}

            <label className={classes.label}>Password</label>
            <input
              className={classes.input}
              type="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              name="password"
            />
            {errors.password && <div>{errors.password}</div>}

            <button className={classes.button} type="submit">
              Sign In
            </button>

            <div className={classes.title}>
              Not a member?
              <Link href="/auth/users/sign-up">
                <a>Sign Up</a>
              </Link>
            </div>
          </form>
        )}
      </Formik>
    </AuthLayout>
  );
}

SignIn.getInitialProps = async (context) => {
  const { req, res } = context;
  const session = await getSession({ req });

  if (session && res && session.accessToken) {
    res.writeHead(302, {
      Location: "/",
    });
    res.end();
    return;
  }

  return {
    session: undefined,
    providers: await providers(context)
  };
};


export default SignIn
