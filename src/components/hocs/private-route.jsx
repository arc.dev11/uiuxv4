import { useEffect } from 'react'
import { signIn, useSession } from 'next-auth/client'

const PrivateRoute = Component => ({ ...props }) => {

    const [session, loading] = useSession()

    useEffect(() => {
        if (!session && !loading) signIn()
    }, [session, loading])

    if (session) {
        return <Component {...props} />
    }
    else return null

};

export default PrivateRoute