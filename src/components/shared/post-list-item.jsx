import React from 'react';
import { makeStyles, createStyles } from "@material-ui/core/styles";
import Image from 'next/image'
import { Avatar } from '@material-ui/core'
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import CommentIcon from '@material-ui/icons/Comment';
import BookmarkIcon from '@material-ui/icons/Bookmark';

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            backgroundColor: '#FFFFFF',
            boxShadow: '0px 10px 11px rgba(0, 0, 0, 0.15)',
            borderRadius: '10px',
            margin: theme.spacing(1)
        },
        imagesWrapper: {
            height: 300,
            width: 400
        },
        profileWrapper: {
            display: 'flex',
            flexDirection: 'row',
            padding: theme.spacing(1),
            alignItems: 'center'
        },
        profileInfo: {
            display: 'flex',
            flexDirection: 'column',
            padding: theme.spacing(1)
        },
        profileTitle: {
            fontStyle: 'normal',
            fontWeight: 'bold',
            fontSize: 18,
            color: '#CE0F3D'
        },
        profileSubTitle: {
            fontStyle: 'normal',
            fontWeight: '300',
            fontSize: 16
        },
        footer: {
            padding: theme.spacing(1),
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            '& svg': {
                color: '#707070',
                margin: theme.spacing(1),
                cursor: 'pointer'
            }
        },
        footerLeft: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            fontStyle: 'normal',
            fontWeight: 300,
            fontSize: 16,
            textAlign: 'center',
            color: '#707070'
        }
    })
);

function PostItem({ id, title, subtitle, images, likes, comments, handleClick }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div
                onClick={() => handleClick(id)}
                className={classes.imagesWrapper}>
                <Image
                    width={400}
                    height={300}
                    layout="responsive"
                    alt={title}
                    src={images[0].src} />
            </div>
            <div className={classes.profileWrapper}>
                <Avatar>H</Avatar>
                <div className={classes.profileInfo}>
                    <div className={classes.profileTitle}>{title}</div>
                    <div className={classes.profileSubTitle}>{subtitle}</div>
                </div>
            </div>
            <div className={classes.footer}>
                <div className={classes.footerLeft}>
                    <ThumbUpIcon />{likes}<CommentIcon />{comments.length}
                </div>
                <BookmarkIcon />
            </div>
        </div>
    );
}

export default PostItem