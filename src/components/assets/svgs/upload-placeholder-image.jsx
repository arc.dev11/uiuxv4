const UploadPlaceHolderImage = ({ className, color = "white", width = 200, height = 134 }) => (
  <span className={className}>
    <svg width={width} height={height} viewBox="0 0 200 134" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M161.25 50.6668C155.583 21.9168 130.333 0.333496 100 0.333496C75.9167 0.333496 55 14.0002 44.5833 34.0002C19.5 36.6668 0 57.9168 0 83.6668C0 111.25 22.4167 133.667 50 133.667H158.333C181.333 133.667 200 115 200 92.0002C200 70.0002 182.917 52.1668 161.25 50.6668ZM116.667 75.3335V108.667H83.3333V75.3335H58.3333L100 33.6668L141.667 75.3335H116.667Z" fill={color} />
    </svg>
  </span>
);

export default UploadPlaceHolderImage