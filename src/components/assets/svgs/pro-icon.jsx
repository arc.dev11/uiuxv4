const ProIcon = ({ color, className }) => {
  return (
    <span
      className={className}
      style={{
        position: 'relative',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
      <span style={{ position: 'absolute', color: '#ffffff', fontWeight: 700, fontSize: 9, fontFamily: 'Quicksand' }}>Pro</span>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M12 0L15.994 4.119L21.624 4.754L20.974 10.525L24 15.434L19.2 18.511L17.341 24L12 22.066L6.659 24L4.8 18.512L0 15.435L3.026 10.525L2.376 4.754L8.006 4.119L12 0Z" fill={color} />
      </svg>
    </span>
  )
};

export default ProIcon