const CircleCheckIcon = ({ color="none", className }) => (
  <span className={className}>
    <svg width="20" height="20" viewBox="0 0 20 20" fill={color} xmlns="http://www.w3.org/2000/svg">
      <path d="M14.59 5.58067L8 12.1707L4.41 8.59067L3 10.0007L8 15.0007L16 7.00067L14.59 5.58067ZM10 0.000671387C4.48 0.000671387 0 4.48067 0 10.0007C0 15.5207 4.48 20.0007 10 20.0007C15.52 20.0007 20 15.5207 20 10.0007C20 4.48067 15.52 0.000671387 10 0.000671387ZM10 18.0007C5.58 18.0007 2 14.4207 2 10.0007C2 5.58067 5.58 2.00067 10 2.00067C14.42 2.00067 18 5.58067 18 10.0007C18 14.4207 14.42 18.0007 10 18.0007Z" fill="#C8C8C8" />
    </svg>
  </span>
);

export default CircleCheckIcon