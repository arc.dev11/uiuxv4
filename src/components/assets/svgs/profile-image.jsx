const ProfileImage = ({ className, onClick }) => {
  return (
    <span className={className} onClick={onClick}>
      <svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="28" cy="28" r="28" fill="#F3F3F3" />
        <path d="M28 8C16.96 8 8 16.96 8 28C8 39.04 16.96 48 28 48C39.04 48 48 39.04 48 28C48 16.96 39.04 8 28 8ZM28 14C31.32 14 34 16.68 34 20C34 23.32 31.32 26 28 26C24.68 26 22 23.32 22 20C22 16.68 24.68 14 28 14ZM28 42.4C23 42.4 18.58 39.84 16 35.96C16.06 31.98 24 29.8 28 29.8C31.98 29.8 39.94 31.98 40 35.96C37.42 39.84 33 42.4 28 42.4Z" fill="#CE0F3D" />
      </svg>
    </span>
  )
};

export default ProfileImage