const DraftIcon = ({ color, className }) => (
  <span className={className}>
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M21 12L16.63 18.16C16.4445 18.4193 16.1999 18.6308 15.9164 18.7768C15.633 18.9229 15.3189 18.9994 15 19H12V13H9V10H3V7.00003C3.00158 6.47008 3.2128 5.96229 3.58753 5.58756C3.96227 5.21283 4.47005 5.00161 5 5.00003H15C15.3193 4.99841 15.6342 5.07393 15.918 5.22019C16.2018 5.36644 16.446 5.57908 16.63 5.84003L21 12ZM10 15H7V12H5V15H2V17H5V20H7V17H10V15Z"  fill={color} />
    </svg>
  </span>
);

export default DraftIcon