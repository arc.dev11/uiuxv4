import React from 'react';
import useAutocomplete from '@material-ui/lab/useAutocomplete';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

import { Item } from './item';
import KeyboardArrowDownIcon from 'src/components/assets/svgs/keyboard-arrow-down-icon'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            userSelect: 'none',
            marginLeft: 15,
            '& .input-option:checked+li': {
                color: theme.palette.primary.main
            }
        },
        inputWrapper: {
            width: 190,
            height: 50,
            position: 'relative',
            display: 'flex',
            alignItems: 'center',
            padding: '5px 15px',
            backgroundColor: theme.palette.secondary.main
        },
        input: {
            color: theme.palette.primary.main,
            userSelect: 'none',
            fontSize: 15,
            fontWeight: 700,
            cursor: 'pointer'
        },
        arrowIcon: {
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-50%)',
            right: 15,
            cursor: 'pointer'
        },
        listbox: {
            width: 190,
            margin: 0,
            zIndex: 1,
            position: 'absolute',
            listStyle: 'none',
            backgroundColor: theme.palette.secondary.main,
            overflow: 'auto',
            maxHeight: 200,
            padding: '5px 15px',
            borderRadius: '0px 0px 25px 25px',
            '& li[data-focus="true"]': {
                cursor: 'pointer',
                color: theme.palette.primary.main
            },
            '& li:active': {
                color: theme.palette.primary.main
            }
        }
    })
);

export default function SelectBox() {
    const classes = useStyles();

    const {
        getRootProps,
        getInputProps,
        getListboxProps,
        getOptionProps,
        popupOpen,
        groupedOptions,
    } = useAutocomplete({
        options: Options,
        getOptionLabel: (option) => option.title,
        defaultValue: Options[0]
    });

    const items = React.Children.toArray(groupedOptions.map((option, index) => (
        <Item getOptionProps={getOptionProps} option={option} index={index} />
    )))

    return (
        <div className={classes.root}>
            <div className={classes.inputWrapper} {...getRootProps()} style={{ borderRadius: popupOpen ? '25px 25px 0px 0px' : 25 }}>
                <input className={classes.input} readOnly {...getInputProps()} />
                <div className={classes.arrowIcon} {...getInputProps()}>
                    <KeyboardArrowDownIcon color="#ce0f3d" className="" />
                </div>
            </div>
            {groupedOptions.length > 0 ? (
                <ul className={classes.listbox} {...getListboxProps()}>
                    {items}
                </ul>
            ) : null}
        </div>
    );
}

const Options = [
    {
        value: '1',
        title: 'Employee Account',
        tag: ''
    },
    {
        value: '2',
        title: 'Agency Account ',
        tag: 'Coming Soon'
    }
];
