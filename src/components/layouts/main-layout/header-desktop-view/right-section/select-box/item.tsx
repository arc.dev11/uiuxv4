import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
        },
        item: {
            fontSize: 15,
            fontWeight: 700,
            color: '#707070',
            margin: '10px 0px'
        },
        tag: {
            maxWidth: 40,
            color: theme.palette.primary.main,
            fontSize: 7,
            fontWeight: 700,
            textAlign: 'center'
        }
    })
);

export function Item({getOptionProps, option, index}: any) {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <li {...getOptionProps({ option, index })} id={`${option.value}select-item`} className={classes.item}>{option.title}</li>
            <span className={classes.tag}>{option.tag}</span>
        </div>

    );
}
