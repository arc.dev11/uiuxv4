import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { useSession } from 'next-auth/client';

import SearchBox from './search-box';
import EmailIcon from 'src/components/assets/svgs/email-icon';
import { Profile } from './profile';
import SelectBox from './select-box';
import { LoginSignUp } from './login-sign-up';


const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexGrow: 1,
        '& .go-pro': {
            fontSize: 14,
            fontWeight: 700,
            cursor: 'pointer',
            textAlign: 'center',
            color: theme.palette.primary.main
        },
        '& .vertical-divider': {
            height: '22.5px',
            borderRight: `solid 1px ${theme.palette.secondary.dark}`,
            margin: '0px 15px'
        },
        '& .upload-button': {
            maxWidth: 120,
            height: 40,
            textTransform: 'capitalize',
            borderRadius: 25
        },
        '& .email-icon': {
            marginLeft: 15,
            cursor: 'pointer'
        }
    }
}))

export default function RightSection() {

    const classes = useStyles();
    const [session] = useSession();

    const loginSignUp = React.Children.toArray(loginRegister?.map((item) => <LoginSignUp item={item} />))

    return (
        <div className={classes.root}>
            <SearchBox />
            {session
                ?
                <>
                    <div className="go-pro">Go Pro</div>
                    <div className="vertical-divider"></div>
                    <Button variant="contained" color="primary" className="upload-button">Upload</Button>
                    <EmailIcon className="email-icon" />
                    <Profile />
                    <SelectBox />
                </>
                :
                <>
                    <div className="vertical-divider"></div>
                    {loginSignUp}
                </>
            }
        </div>
    )
}

const loginRegister = [
    {
        id: '1',
        title: 'Login'
    },
    {
        id: '2',
        title: 'Sign Up'
    }
]