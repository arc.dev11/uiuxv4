import React from 'react';
import useAutocomplete from '@material-ui/lab/useAutocomplete';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

import SearchlIcon from 'src/components/assets/svgs/search-icon';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            userSelect: 'none',
            marginLeft: 15,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
            flexGrow: 1,
            margin: '0px 10px',
            '& .input-option:checked+li': {
                color: theme.palette.primary.main
            }
        },
        inputWrapper: {
            height: 50,
            position: 'relative',
            display: 'flex',
            padding: '5px 15px',
            backgroundColor: theme.palette.secondary.main,
            [theme.breakpoints.down('md')]: {
                width: '260px !important'
            }
        },
        input: {
            paddingLeft: 23,
            color: theme.palette.primary.main,
            userSelect: 'none',
            fontSize: 15,
            fontWeight: 700
        },
        arrowIcon: {
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-50%)',
            left: 15,
            cursor: 'pointer',
            fontSize: 28
        },
        listbox: {
            width: 280,
            display: 'felx',
            justifyContent: 'scratch',
            flexDirection: 'column',
            flexGrow: 1,
            margin: 0,
            zIndex: 1,
            position: 'absolute',
            top: 70,
            listStyle: 'none',
            backgroundColor: theme.palette.secondary.main,
            overflow: 'auto',
            maxHeight: 200,
            padding: '5px 15px',
            borderRadius: '0px 0px 25px 25px',
            '& li[data-focus="true"]': {
                cursor: 'pointer',
                color: theme.palette.primary.main
            },
            '& li:active': {
                color: theme.palette.primary.main
            },
            [theme.breakpoints.down('md')]: {
                width: '260px !important'
            }
        }
    })
);

export default function SelectBox() {
    const classes = useStyles();

    const {
        getRootProps,
        getInputProps,
        getListboxProps,
        getOptionProps,
        popupOpen,
        groupedOptions,
    } = useAutocomplete({
        options: Options,
        getOptionLabel: (option) => option.title
    });

    const items = React.Children.toArray(groupedOptions.map((option, index) => (
        <div>{option.title}</div>
    )))

    const borderRadius = popupOpen ? '25px 25px 0px 0px' : 25
    const width = popupOpen ? 280 : 220

    return (
        <div className={classes.root}>
            <div className={classes.inputWrapper} {...getRootProps()} style={{ borderRadius, width }}>
                <input className={classes.input} {...getInputProps()} placeholder="Search" />
                <div className={classes.arrowIcon} {...getInputProps()}>
                    <SearchlIcon color="" className="" />
                </div>
            </div>
            {groupedOptions.length > 0 ? (
                <ul className={classes.listbox} {...getListboxProps()}>
                    {items}
                </ul>
            ) : null}
        </div>
    );
}

const Options = [
    {
        value: '1',
        title: 'Employee Account',
        tag: ''
    },
    {
        value: '2',
        title: 'Agency Account ',
        tag: 'Coming Soon'
    }
];
