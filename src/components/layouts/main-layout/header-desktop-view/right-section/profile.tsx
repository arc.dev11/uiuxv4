import React, { useState } from 'react';
import { makeStyles, createStyles, Menu, MenuItem, withStyles } from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        marginLeft: 15,
        cursor: 'pointer'
    },
    profileIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 3,
        borderRadius: 25,
        backgroundColor: theme.palette.secondary.main,
        borderBottom: `1px solid ${theme.palette.secondary.main}`,
        '& > svg': {
            fontSize: 43
        }
    },
    menu: {
        width: 250,
        borderRadius: 25,
        '& > ul': {
            width: 'unset !important'
        }
    }
}))

const StyledMenuItem = withStyles((theme: any) => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        fontSize: 15,
        fontWeight: 700,
        '&:first-child': {
            borderBottom: `2px solid ${theme.palette.secondary.main}`
        },
        '&:focus': {
            color: theme.palette.primary.main
        },
    },
}))(MenuItem);

export function Profile() {

    const classes = useStyles()
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const handleClick = (event: any) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <div className={classes.profileIcon} onClick={handleClick}>
                <AccountCircleIcon color="primary" />
            </div>

            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                classes={{ paper: classes.menu }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <StyledMenuItem onClick={handleClose}>
                    <div>Amanda Wells</div>
                    <div className={classes.profileIcon}>
                        <AccountCircleIcon color="primary" />
                    </div>
                </StyledMenuItem>
                <StyledMenuItem onClick={handleClose}>My Profile</StyledMenuItem>
                <StyledMenuItem onClick={handleClose}>Profile Settings</StyledMenuItem>
                <StyledMenuItem onClick={handleClose}>Logout</StyledMenuItem>
            </Menu>
        </div>
    )
}