import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core';


import LogoIcon from 'src/components/assets/svgs/logo-icon';
import { NavItem } from './nav-item';

const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        display: 'flex',
        alignItems: 'center'
    },
    logo: {
        marginRight: 20
    },
    navBar: {
        display: 'flex',
        alignItems: 'center'
    }
}))

export default function LeftSection() {

    const classes = useStyles()

    const navBar = React.Children.toArray(navItems?.map(item => <NavItem item={item} />))

    return (
        <div className={classes.root}>
            <LogoIcon className={classes.logo} />
            <div className={classes.navBar}>{navBar}</div>
        </div>
    )
}

const navItems = [
    {
        id: '1',
        title: 'Inspiration'
    },
    {
        id: '2',
        title: 'Projects'
    },
    {
        id: '3',
        title: 'Job posts'
    },
    {
        id: '4',
        title: 'Market'
    }
]