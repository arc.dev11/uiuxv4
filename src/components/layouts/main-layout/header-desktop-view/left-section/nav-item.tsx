import { createStyles, makeStyles } from '@material-ui/core';


const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        cursor: 'pointer',
        textAlign: 'center',
        color: theme.palette.secondary.dark,
        '& > input': {
            display: 'none' 
        },
        '& input:checked+span': {
            color: theme.palette.primary.main
        },
        '&:not(&:first-child)': {
            margin: '0px 8px'
        }
    },
    title: {
        fontSize: 16,
        fontWeight: 700
    }
}))

export function NavItem({ item }: any) {

    const classes = useStyles()

    const handleClick = () => {
    }

    return (
        <label htmlFor={`${item.id}nav-item`} className={classes.root}>
            <input id={`${item.id}nav-item`} type="radio" name="nav-item" defaultChecked={item.id === '1' ? true : false} />
            <span onClick={handleClick} className={classes.title}>{item.title}</span>
        </label>
    )
}