import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core';

import LeftSection from './left-section';
import RightSection from './right-section';


const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        marginBottom: 30,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
}))

export default function HeaderDesktopView() {

    const classes = useStyles()

    return (
        <div className={classes.root}>
            <LeftSection />
            <RightSection />
        </div>
    )
}