import { createStyles, Hidden, makeStyles } from "@material-ui/core";

import Footer from "./footer";
import HeaderDesktopView from "./header-desktop-view";

const useStyles = makeStyles((theme: any) => createStyles({
    container: {
        padding: '20px 4% 40px 4%',
        [theme.breakpoints.down('sm')]: {
            padding: '20px 2% 40px 2%'
        }
    }
}))

export default function MainLayout({ children }: any) {

    const classes = useStyles()

    return (
        <div>
            <div className={classes.container}>
                <Hidden mdDown implementation="css">
                    <HeaderDesktopView />
                </Hidden>

                {children}
            </div>
            <Footer />
        </div>
    )
}