import { createStyles, makeStyles } from '@material-ui/core';

import BottomSection from './bottom-section';
import TopSection from './top-section';
import Navigation from './navigation';


const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        position: 'relative'
    }
}))

export default function Footer() {

    const classes = useStyles()

    return (
        <div className={classes.root}>
            <TopSection />
            <BottomSection />
            <Navigation />
        </div>
    )
}