import { createStyles, makeStyles, Container } from '@material-ui/core';


const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 60,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 80,
        backgroundColor: theme.palette.primary.main,
        [theme.breakpoints.up('md')]: {
            marginBottom: 0,
        }
    },
    copyRight: {
        fontSize: 14,
        color: 'white',
        textAlign: 'center'
    }
}))

export default function BottomSection() {

    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Container>
                <div className={classes.copyRight}>Copyright ©  2020 UiUxCENTER Company. All rights reserved.</div>
            </Container>
        </div>
    )
}