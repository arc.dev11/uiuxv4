import { createStyles, makeStyles, Avatar } from '@material-ui/core';

import HomeIcon from 'src/components/assets/svgs/home-icon';
import WorkIcon from 'src/components/assets/svgs/work-icon';
import UploadIcon from 'src/components/assets/svgs/upload-icon';
import NotificationIcon from 'src/components/assets/svgs/notification-icon';
import { NavIcon } from './nav-icon';

const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        position: 'fixed',
        bottom: 0,
        left: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 80,
        width: '100%',
        zIndex: 200,
        padding: '0px 4%',
        backgroundColor: 'white',
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
        '& .profileImage': {
            width: 30,
            height: 30,
            border: `3px solid ${theme.palette.secondary.dark}`
        }
    },

}))

export default function Navigation() {

    const classes = useStyles()

    return (
        <div className={classes.root}>
            <NavIcon index="1">
                <HomeIcon />
            </NavIcon>

            <NavIcon index="2">
                <WorkIcon />
            </NavIcon>

            <NavIcon index="3">
                <UploadIcon />
            </NavIcon>

            <NavIcon index="4">
                <NotificationIcon />
            </NavIcon>

            <NavIcon index="5">
                <Avatar className="profileImage">M</Avatar>
            </NavIcon>
        </div>
    )
}