import { createStyles, makeStyles } from '@material-ui/core';


const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        cursor: 'pointer',
        textAlign: 'center',
        whiteSpace: 'nowrap',
        color: theme.palette.secondary.dark,
        '& > input': {
            display: 'none'
        },
        '& input:checked+.icon path': {
            fill: theme.palette.primary.main
        },
        '& input:checked+.icon .profileImage': {
            border: `3px solid ${theme.palette.primary.main}`
        }
    }
}))

export function NavIcon({ index, children }: any) {

    const classes = useStyles()

    const handleClick = () => {
    }

    return (
        <label htmlFor={`${index}nav-icon`} className={classes.root}>
            <input id={`${index}nav-icon`} type="radio" name="nav-icon" defaultChecked={index === '1' ? true : false} />
            <span className="icon">{children}</span>
        </label>
    )
}