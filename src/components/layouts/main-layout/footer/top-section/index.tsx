import React from 'react';
import { createStyles, makeStyles, Container, Grid } from '@material-ui/core';

import LinkedinIcon from 'src/components/assets/svgs/linkdein-footer-icon';
import FacebookIcon from 'src/components/assets/svgs/facebook-icon';
import InstagramIcon from 'src/components/assets/svgs/instagram-icon';
import { NavPage } from './nav-page';
import { SocialNetworkIcon } from './social-network-icon';

const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        display: 'flex',
        alignItems: 'center',
        padding: '5px 10px',
        minHeight: 100,
        backgroundColor: theme.palette.secondary.main
    },
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10
    },
    navItemsBox: {
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        [theme.breakpoints.down('xs')]: {
            justifyContent: 'center'
        }
    },
    SocialNetworkIconsBox: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        [theme.breakpoints.down('xs')]: {
            justifyContent: 'center',
            marginTop: 10
        }
    }
}))

export default function TopSection() {

    const classes = useStyles()

    const navBar = React.Children.toArray(navItems?.map(item => <NavPage item={item} />))

    const socialNetworkIcons = React.Children.toArray(socialNetworksIcons?.map(icon => <SocialNetworkIcon icon={icon} />))

    return (
        <div className={classes.root}>
            <Container maxWidth="lg" className={classes.container}>
                <Grid container>
                    <Grid item xs={12} sm={8} className={classes.navItemsBox}>{navBar}</Grid>
                    <Grid item xs={12} sm={4} className={classes.SocialNetworkIconsBox}>{socialNetworkIcons}</Grid>
                </Grid>
            </Container>
        </div>
    )
}

const navItems = [
    {
        id: '1',
        title: 'Go Pro'
    },
    {
        id: '2',
        title: 'About Us'
    },
    {
        id: '3',
        title: 'Contact Us'
    },
    {
        id: '4',
        title: 'Privacy Policy'
    },
    {
        id: '5',
        title: 'Terms of Service'
    }

]

const socialNetworksIcons = [
    {
        id: '1',
        component: <LinkedinIcon className="icon" />,
        link: '/profile'
    },
    {
        id: '2',
        component: <InstagramIcon className="icon" />,
        link: '/profile'
    },
    {
        id: '3',
        component: <FacebookIcon className="icon" />,
        link: '/profile'
    }
]