import { createStyles, makeStyles } from '@material-ui/core';


const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        cursor: 'pointer',
        textAlign: 'center',
        whiteSpace: 'nowrap',
        marginTop: 5,
        marginBottom: 5,
        color: theme.palette.secondary.dark,
        '& > input': {
            display: 'none' 
        },
        '& input:checked+span': {
            color: theme.palette.primary.main
        },
        '&:not(&:last-child)': {
            marginRight: 15
        }
    },
    title: {
        fontSize: 16,
        fontWeight: 700
    }
}))

export function NavPage({ item }: any) {

    const classes = useStyles()

    const handleClick = () => {
    }

    return (
        <label htmlFor={`${item.id}nav-page`} className={classes.root}>
            <input id={`${item.id}nav-page`} type="radio" name="nav-page" defaultChecked={item.id === '1' ? true : false} />
            <span onClick={handleClick} className={classes.title}>{item.title}</span>
        </label>
    )
}