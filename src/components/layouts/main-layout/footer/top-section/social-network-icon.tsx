import { createStyles, makeStyles } from '@material-ui/core';
import Link from 'next/link';

const useStyles = makeStyles((theme: any) => createStyles({
    root: {
        cursor: 'pointer',
        textAlign: 'center',
        color: theme.palette.secondary.dark,
        '& > input': {
            display: 'none' 
        },
        '& > input:checked+.icon > svg': {
            color: theme.palette.primary.main,
            fontWeight: 700
        },
        '&:not(&:first-child)': {
            margin: '0px 8px'
        },
        '& .icon': {
            margin: '0px 5px',
        },
        '& .icon > svg': {
            width: 40,
            height: 40
        }
    }
}))

export function SocialNetworkIcon({ icon }: any) {

    const classes = useStyles()

    const handleClick = () => {
    }

    return (
        <label htmlFor={`${icon.id}social-network`} className={classes.root}>
            <input id={`${icon.id}social-network`} type="radio" name="social-network" defaultChecked={icon.id === '1' ? true : false} />
            <Link href={icon.link}><a>{icon.component}</a></Link>
        </label>
    )
}