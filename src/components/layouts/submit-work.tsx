import React from "react";
import LogoImage from "../assets/svgs/logo-image";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexDirection: "column",
    },
    header: {
      display: "flex",
      flexDirection: "row",
      [theme.breakpoints.down('sm')]:{
        flexDirection: "column",
        alignItems:'center',
      },
      padding: theme.spacing(2),
    },
    title: {
      padding: theme.spacing(1),
      textAlign: "center",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: 22,
      color: "#CE0F3D",
      margin: "auto",
    },
    content: {
      padding: theme.spacing(3)
    },
  })
);

const SubmitWorkLayout = ({ children }: any) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <LogoImage />
        <div className={classes.title}>Publish your Work</div>
      </div>
      <div className={classes.content}>{children}</div>
    </div>
  );
};

export default SubmitWorkLayout;
