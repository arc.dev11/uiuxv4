import React from 'react'
import AuthBesideImage from '../assets/svgs/auth-beside-image'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: "flex",
      flexDirection: "row",
    },
    formWrapper: {
      flexGrow: 1,
      minHeight: '99vh',
      [theme.breakpoints.down('sm')]: {
        maxWidth: '97vw',
        minWidth: '97vw'
      }
    },
    besideWrapper: {
      backgroundColor: '#ce0f3d',
      maxWidth: '49vw',
      minWidth: '49vw',
      [theme.breakpoints.down('sm')]: {
        display: 'none'
      }
    }
  })
);

const AuthLayout = ({ children }) => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.formWrapper}>
        {children}
      </div>
      <div className={classes.besideWrapper}>
        <AuthBesideImage />
      </div>
    </div>
  )
}


export default AuthLayout
