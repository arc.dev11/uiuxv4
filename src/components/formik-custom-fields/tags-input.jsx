import { useEffect, useState } from "react"
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'inline-flex',
      flexWrap: 'wrap',
      justifyContent: 'flex-start',
      alignItems: 'center',
      padding: 20,
      background: '#f3f3f3',
      borderRadius: 28,
      minHeight: 56,
      width: '100%'
    },
    input: {
      flexGrow: 1,
      border: 'none',
      outline: 'none'
    },
    tag: {
      background: '#ce0f3d',
      borderRadius: 15,
      margin: 5,
      padding: 5,
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: 16,
      color: '#ffffff'
    },
    delete: {
      display: 'inline-block',
      margin: 'auto',
      marginLeft: 5,
      width: 20,
      height: 20,
      color: 'white',
      fontSize: 15,
      fontWeight: 'bold',
      cursor: 'pointer',
      border: 'none',
      outline: 'none',
      userSelect: 'none'
    }
  })
);


const TagsInput = ({ value = [], onChange }) => {

  const classes = useStyles()
  const [newTag, setNewTag] = useState('')
  const [tags, setTags] = useState(value)

  useEffect(() => {
    if (onChange) onChange(tags)
  }, [tags])

  useEffect(() => {
    setTags(value)
  }, [value])

  const handleChange = e => {
    setNewTag(() => e.target.value)
  }

  const handleKeyDown = e => {

    if (e.keyCode === 13 && e.target.value !== '') {
      let myNewTag = newTag.trim()

      if (tags.indexOf(myNewTag) === -1) {
        setTags((odt => [...odt, myNewTag]))
        setNewTag(() => '')
      }
      e.target.value = ''
    }
  }

  const handleRemoveTag = tag => {
    setTags(tags => tags.filter(el => !tag.includes(el)))
    setNewTag(() => '')
  }

  const Tag = props => <span className={classes.tag} {...props} />
  const Delete = props => <button className={classes.delete} {...props}>x</button>

  return (
    <div className={classes.root}>
      {tags.map((tag, index) => (
        <Tag key={index}>
          {tag}
          <Delete
            onClick={() => handleRemoveTag(tag)}
            type="button" />
        </Tag>
      ))}
      <input
        className={classes.input}
        type="text"
        onChange={handleChange}
        onKeyDown={handleKeyDown} />
    </div>
  )
}

export default TagsInput