import React, { useEffect, useRef, useState } from 'react'
import DropDownListItem from './item'
import SearchInput from './search-input'
import KeyboardArrowDownIcon from '../../../components/assets/svgs/keyboard-arrow-down-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";
import clsx from 'clsx';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      position: 'relative',
      paddingLeft: 20,
      paddingRight: 20,
      background: '#f3f3f3',
      borderRadius: 28,
      height: 56,
      width: '100%',
      color: '#ce0f3d'
    },
    list: {
      zIndex: 2,
      padding: 30,
      borderRadius: '0px 0px 20px 20px',
      background: '#f3f3f3',
      position: 'absolute',
      left: 0,
      right: 0,
      top: '61%'
    },
    button: {
      color: 'inherit',
      width: '100%',
      height: '100%',
      fontStyle: 'normal',
      fontweight: 300,
      fontSize: 16,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }
  })
);

const SingleSelect = ({
  data = [],
  toggleChangeListItem = () => { },
  removeChangeListItem = () => { },
  uniqueKey = "key",
  selected = null,
  classes: userClasses
}) => {

  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const wrapper = useRef()

  useEffect(() => {
    document.addEventListener("mousedown", handleDocClick, false);
    return () => {
      document.removeEventListener("mousedown", handleDocClick, false);
    }
  }, [])

  useEffect(() => {
    toggleOpen(false)
  }, [selected])

  const toggleOpen = value => {
    setOpen(() => value)
  };

  const handleDocClick = e => {
    if (!wrapper.current.contains(e.target)) {
      toggleOpen(false)
    }
  };

  const handleKeyDown = e => {
    if (e.keyCode === 8 && e.target.value === '') {
      removeChangeListItem()
    }
  }

  const handleSearchInputChange = e => {

  }

  const renderSelected = () => {


    let labelContent = data.find(el => el.value === selected)?.label;
    if (!labelContent) {
      labelContent = "Select";
    }

    return (
      <div
        onClick={() => toggleOpen(true)}
        className={classes.button}>
        {open ? (
          <SearchInput
            styles={{ root: { zIndex: open ? 3 : 1 } }}
            onChange={handleSearchInputChange}
            onFocus={() => toggleOpen(true)}
            handleKeyDown={handleKeyDown}
            handleRemove={removeChangeListItem}
            data={data}
            selected={selected} />
        ) : (
          <>{labelContent}</>
        )}

        <KeyboardArrowDownIcon color="#ce0f3d" />
      </div>
    );
  };

  const renderDropDownList = () => {

    let data_ = [...data];
    return data_.map((listData, index) => {
      return (
        <DropDownListItem
          key={index}
          toggleChangeListItem={toggleChangeListItem}
          listData={listData}
          uniqueKey={uniqueKey}
        />
      );
    });
  };
  return (
    <div className={clsx(classes.root, userClasses?.root)} ref={wrapper}>
      {renderSelected()}
      {open && (
        <div className={classes.list}>
          {renderDropDownList()}
        </div>
      )}
    </div>
  );
}


export default SingleSelect