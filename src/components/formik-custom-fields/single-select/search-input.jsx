import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'inline-flex',
      flexWrap: 'wrap',
      width: '100%',
      '& input': {
        border: 'none',
        outline: 'none',
        flexGrow: 1
      }
    }
  })
);


const SearchInput = ({ styles, handleKeyDown, onFocus, onBlur, onChange }) => {

  const classes = useStyles()
  return (
    <div style={styles.root} className={classes.root}>
      <input
        type="text"
        onKeyDown={handleKeyDown}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
      />
    </div>
  )
}

export default SearchInput