import React, { useEffect, useRef, useState } from 'react'
import DropDownListItem from './item'
import SearchInput from './search-input'
import KeyboardArrowDownIcon from '../../../components/assets/svgs/keyboard-arrow-down-icon'
import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      position: 'relative',
      paddingLeft: 20,
      paddingRight: 20,
      background: '#f3f3f3',
      borderRadius: 28,
      height: 56,
      width: '100%',
      color: '#ce0f3d'
    },
    list: {
      zIndex: 2,
      padding: 30,
      borderRadius: '0px 0px 20px 20px',
      background: '#f3f3f3',
      position: 'absolute',
      left: 0,
      right: 0,
      top: '61%'
    },
    button: {
      color: 'inherit',
      width: '100%',
      height: '100%',
      fontStyle: 'normal',
      fontweight: 300,
      fontSize: 16,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    }
  })
);

const NewDropDown = ({
  data = [],
  toggleChangeListItem = () => { },
  removeChangeListItem = () => { },
  uniqueKey = "key",
  selected = []
}) => {

  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const wrapper = useRef()

  useEffect(() => {
    document.addEventListener("mousedown", handleDocClick, false);
    return () => {
      document.removeEventListener("mousedown", handleDocClick, false);
    }
  }, [])

  const toggleOpen = value => {
    setOpen(() => value)
  };

  const handleDocClick = e => {
    if (!wrapper.current.contains(e.target)) {
      setOpen(() => false)
    }
  };

  const handleKeyDown = e => {
    if (e.keyCode === 8 && e.target.value === '') {
      removeChangeListItem(selected.pop())
    }
  }

  const handleSearchInputChange = e => {

  }

  const renderSelected = () => {


    let labelContent = "";
    if (!selected.length) {
      labelContent = "None Selected";
    } else if (selected.length === data.length) {
      labelContent = "All Selected";
    } else if (selected.length === 1) {
      const selectedOne = data.find(item => item[uniqueKey] === selected[0]);
      labelContent = selectedOne.label;
    } else {
      labelContent = `${selected.length} Selected`;
    }

    return (
      <div
        className={classes.button}>
        <SearchInput
          styles={{ root: { zIndex: open ? 3 : 1 } }}
          onChange={handleSearchInputChange}
          onFocus={() => toggleOpen(true)}
          handleKeyDown={handleKeyDown}
          handleRemove={removeChangeListItem}
          data={data}
          selected={selected} />
        <KeyboardArrowDownIcon color="#ce0f3d" />
      </div>
    );
  };

  const renderDropDownList = () => {

    let data_ = [...data];

    const getIsChecked = ({ listData, uniqueKey, selected }) => {
      let isChecked = false;
      isChecked = selected.indexOf(listData[uniqueKey]) > -1;
      return isChecked;
    };

    return data_.map((listData, index) => {
      const isChecked = getIsChecked({ listData, uniqueKey, selected });
      return (
        <DropDownListItem
          key={index}
          toggleChangeListItem={toggleChangeListItem}
          listData={listData}
          uniqueKey={uniqueKey}
          isChecked={isChecked}
        />
      );
    });
  };
  return (
    <div className={classes.root} ref={wrapper}>
      {renderSelected()}
      {open && (
        <div className={classes.list}>
          {renderDropDownList()}
        </div>
      )}
    </div>
  );
}


export default NewDropDown