import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'inline-flex',
      flexWrap: 'wrap',
      width: '100%',
      '& input': {
        border: 'none',
        outline: 'none',
        flexGrow: 1
      }
    },
    tag: {
      background: theme.palette.primary.main,
      borderRadius: 15,
      margin: 5,
      padding: 5,
      fontStyle: 'normal',
      fontWeight: 300,
      fontSize: 16,
      color: '#ffffff'
    },
    delete: {
      display: 'inline-block',
      margin: 'auto',
      marginLeft: 5,
      width: 20,
      height: 20,
      color: 'white',
      fontSize: 15,
      fontWeight: 'bold',
      cursor: 'pointer',
      border: 'none',
      outline: 'none',
      userSelect: 'none'
    }
  })
);

const SearchInput = ({ styles, data, selected, handleRemove, handleKeyDown, onFocus, onBlur, onChange }) => {

  const classes = useStyles()
  const allSelected = data.filter(el => selected.includes(el.value))

  const Tag = props => <span className={classes.tag} {...props} />
  const Delete = props => <button className={classes.delete} {...props}>x</button>

  return (
    <div style={styles.root} className={classes.root}>
      {allSelected.map((tag, index) => (
        <Tag key={index}>
          {tag.label}
          <Delete onClick={() => handleRemove(tag.value)} type="button" />
        </Tag>
      ))}
      <input
        type="text"
        onKeyDown={handleKeyDown}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
      />
    </div>
  )
}

export default SearchInput