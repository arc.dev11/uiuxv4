import { makeStyles, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            height: 30,
            display: 'flex',
            alignItems: 'center',
            '& > input': {
                margin: '0.3rem'
            }
        }
    })
);

const DropDownListItem = props => {

    const classes = useStyles()
    const { listData, uniqueKey } = props;

    const toggleChangeListItem = () => {
        props.toggleChangeListItem(listData[uniqueKey]);
    };

    const onKeyUp = e => {
        if (e.keyCode === 13) {
            props.toggleChangeListItem(listData[uniqueKey]);
        }
    };

    const id = `${listData.label}__${listData.value}`;

    return (
        <div
            id={id}
            tabIndex={0}
            className={classes.root}
            onClick={toggleChangeListItem}
            onKeyUp={onKeyUp}
        >
            {listData.label}
        </div>
    );
}

export default DropDownListItem