import { createMuiTheme } from "@material-ui/core/styles";

export default (createMuiTheme as any)({
  typography: {
    button: {
      textTransform: "none",
      fontFamily: "Quicksand",
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "center",
    },
  },
  palette: {
    primary: {
      main: "#ce0f3d",
      light: "#d73f63",
      dark: "#900a2a",
    },
    secondary: {
      main: "#F3F3F3",
      light: "#f5f5f5",
      dark: "#aaaaaa",
    },
  },
  overrides: {
    MuiTextField: {
      root: {
        marginBottom: "5px",
      },
    },
    MuiOutlinedInput: {
      input: {
        "&:-webkit-autofill": {
          WebkitBoxShadow: "0 0 0 30px #FFFFFF inset !important;",
          WebkitTextFillColor: "#000000 !important",
        },
      },
      root: {
        borderRadius: 28,
        backgroundColor: "#FFFFFF",
      },
    },
    MuiButton: {
      label: {
        padding: 5,
      },
      root: {
        borderRadius: 28,
      },
      containedPrimary: {
        color: "#FFFFFF",
      },
      containedSecondary: {
        color: "#ce0f3d",
      },
    },
  },
});
