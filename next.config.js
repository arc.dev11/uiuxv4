const withImages = require('next-images')
module.exports = withImages({
  webpack(config, options) {
    return config
  },
  publicRuntimeConfig: {
    BASE_URL: 'http://localhost:3000'
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  pageExtensions: ['p.tsx', 'p.jsx', 'p.ts', 'p.js']
})